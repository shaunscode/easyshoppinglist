# README #

This app is work in progress as my first serious app I'd like to get out on the internet.

It is a simple shopping list app. I am always walking out the door making lists in an sms to myself and then heading to the shops. At times I want to use the calculator and have a list, and remove items etc, and it can get cumbersome.

I'm not the type to prepare a shopping list in advanced which a lot of other apps out there tend to target. making a list, involving costs or products, quantity etc which is all well and good for the people that do prepare, but I need something a little more simple just to do the job.

Settled with the design of four vieww..

* A catalog for all items in a database that can be scrolled through or filtered and added to or removed from the current shopping list. 
* A list, for all items selected.
* A calculator, for adding up those prices
* An options view, for various settings and options. 

To go a little further into the functionality of the views, I'll explain my goals for each.
###Catalog###
Basically it will store all items. It will be prepopulated with generic items list I get off the internet, but while filtering the scrollable items will diminish until there are none left that match the user input. User can still add the unfound item to their list and it will be stored in the database for next time. 

###List###
This will be the shopping list the user will view while shopping. Items will abe able to be checked off (strike through, ticked, dimmed, or removed complete - user setting) to indicate the item has been handled/collected. 