package com.easy;

import java.util.HashMap;

import com.easy.model.AppModel;
import com.easy.model.ModelServices;
import com.easy.model.ShopItem;

public class TestHarness {

	public static void printModelDetails () {
		
		ModelServices modelServices = new ModelServices(new AppModel());
		
		HashMap<String, ShopItem> items = modelServices.getShopItems();
		
		System.out.println("Model Details\n=======================");
		System.out.println("No.Items: " + items.size());
		System.out.println("Key / DisplayText / ID\n-----------------------");
		for (String key: items.keySet()) {
			ShopItem item = items.get(key);
			System.out.println("" + key + " / " + item.getDisplayText() + " / " + item.getId());
		}
		
	}
	
}
