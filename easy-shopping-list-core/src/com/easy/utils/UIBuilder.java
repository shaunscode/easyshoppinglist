package com.easy.utils;


import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox.CheckBoxStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane.ScrollPaneStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragScrollListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.easy.components.EasyActionHandling;
import com.easy.components.EasyActionType;
import com.easy.framework.EasyAction;
import com.easy.framework.EasyWidget;
import com.easy.model.Assets;
import com.easy.model.Constants;

public class UIBuilder {

	//ui attribute holders
	private static BitmapFont bitmapFont;
	
	private static Color fontColor = Constants.COLOR_LABELS;
	private static Color checkedFontColor = Color.LIGHT_GRAY;
	private static Color downFontColor = Color.LIGHT_GRAY;
	
	private static String displayText = "";
	private static String helpText = "";
	
	private static Drawable drawableUp;
	private static Drawable drawableDown;
	private static Drawable drawableChecked;
	
	private static EasyActionType easyActionType = EasyActionType.VOID;
	private static EasyActionHandling easyActionHandler;
	private static EasyWidget targetWidget;
	
	public static EasyWidget getTargetWidget () {
		return targetWidget;
	}

	public static void setTarget (EasyWidget targetElement) {
		UIBuilder.targetWidget = targetElement;
	}

	private static int currentFontSize = Constants.FONT_SIZE_MD;	
	
	//setters
	public static void setFont (BitmapFont inBitmapFont) {
		bitmapFont = inBitmapFont;
	}
	
	public static void setFontColor (Color c) {
		fontColor = c;
	}
	
	public static void setCheckedFontColor(Color checkedFontColor) {
		UIBuilder.checkedFontColor = checkedFontColor;
	}

	public static void setDownFontColor(Color downFontColor) {
		UIBuilder.downFontColor = downFontColor;
	}

	public static void setDisplayText (String inDisplayText) {
		displayText = inDisplayText;
	}
	
	public static void setDrawableUp (Drawable inDrawableUp) {
		drawableUp = inDrawableUp;
	}
	
	public static void setDrawableDown (Drawable inDrawableDown) {
		drawableDown = inDrawableDown;
	}

	public static void setDrawableChecked (Drawable inDrawableChecked) {
		drawableChecked = inDrawableChecked;
	}

	
	public static void setButtonDrawables (Drawable inDrawableUp, Drawable inDrawableDown, Drawable inDrawableChecked) {
		drawableUp = inDrawableUp;
		drawableDown = inDrawableDown;
		drawableChecked = inDrawableChecked;
	}
	
	public static void setEasyActionType (EasyActionType type) {
		easyActionType = type;
	}

	public static void setEasyActionHandler (EasyActionHandling eventHandler) {
		easyActionHandler = eventHandler;
	}
	
	//gets of ui elements
	public static TextButton getTextButton () {
		TextButton.TextButtonStyle style = new TextButton.TextButtonStyle();
		style.font = bitmapFont;
		style.fontColor = fontColor;
		style.checkedFontColor = checkedFontColor;
		style.downFontColor = downFontColor;
		TextButton tb =  new TextButton(displayText, style);
		addInputEventHandling(tb);
		return tb;
	}
	
	
	public static Label getLabel () {
		Label.LabelStyle style = new Label.LabelStyle(bitmapFont, fontColor);
		Label label = new Label(displayText, style);
		label.setWrap(true);
		addInputEventHandling(label);
		return label;
	}
	
	public static CheckBox getCheckbox () {
		CheckBoxStyle style = new CheckBoxStyle();
		style.checkboxOff = Assets.ICON_TICK;
		style.checkboxOn = Assets.ICON_TICK.tint(Constants.COLOR_GREEN);
		CheckBox checkBox = new CheckBox("", style);
		checkBox.getImageCell().size(40.0f);
		return checkBox;
	}
	
	public static Button getImageButton () {
		ButtonStyle style = new ButtonStyle();
		style.up = drawableUp;
		style.down = drawableDown;
		style.checked = drawableChecked;
		style.disabled = ((SpriteDrawable)drawableUp).tint(Constants.COLOR_LIGHT_GREY);
		Button button = new Button (style);
		addInputEventHandling(button);
		return button;
	}
	
	
	public static TextField getTextField () {
		TextField.TextFieldStyle style = new TextField.TextFieldStyle(Assets.UI_SKIN.get(TextFieldStyle.class));
		style.font = bitmapFont;
		style.fontColor = Color.GRAY;
		style.background = null;
		
		final TextField textField = new TextField("", style);
		textField.setMessageText(displayText);
		addChangeEventHandling (textField);
		
		return textField;
	}
	
	
	public static ScrollPane getScrollPane (Table tableToWrap) {
		
		ScrollPaneStyle style = new ScrollPaneStyle();
//		Drawable bar = Assets.SCROLL_BACKGROUND;
//		bar.setMinWidth(14);
//		style.vScroll = bar;
		Drawable knob = Assets.SCROLL_BAR;
		knob.setMinWidth(14);
		style.vScrollKnob = knob;
		
		//needs to be final for the event handling for scrolling
		final ScrollPane scroller = new ScrollPane(tableToWrap, style);
		scroller.setScrollingDisabled(true, false);
	//	
		
		
		
		/*
		scroller.addListener(new DragScrollListener(scroller) {

			private float oldDY = 0.0f;
			
			private boolean show = true;
			private boolean oldShow = show;
			
			
		    @Override
		    public boolean handle(Event event) {
		    	
		    	System.out.println("Is dragging: " + isDragging());
		    	
		    	
		    	float dY = getDeltaY();
		    	System.out.println("deltaY: " + dY);
		    	
		    	if (dY > oldDY) {
		    		show = false;
		    	} else if (dY < oldDY) {
		    		show = true;
		    	}
		    	
		    	if (show != oldShow) {
		    		EasyAction action;
		    		if (show) {
		    			action = new EasyAction(EasyActionType.CATALOG_SHOW_FILTER);
			    	} else {
			    		action = new EasyAction(EasyActionType.CATALOG_HIDE_FILTER);
			    	}
		    		easyActionHandler.handleAction(action);
		    	}
		    	
		    	
		    	oldShow = show;
		    	oldDY = dY;
				return true;
		    	
		    }
		});
		*/
		
		return scroller;
	}
	
	
	//handling of easyevent firing attached to each button individuallyF
	private static void addInputEventHandling (final Actor targetActor) {

		if (easyActionType.equals(EasyActionType.VOID)) {
			return;
		}
	
		//copy from set details to final forms of event details for handing
		final EasyActionHandling actionHandler = easyActionHandler;
		final EasyActionType actionType = easyActionType;
		final EasyAction action = new EasyAction(actionType);
		
		action.setTarget(targetWidget);
		 
		//userinput action listener (touch, swipe etc) that comes from libgdx
		EventListener eventListener = new ClickListener () {
			 public void clicked(InputEvent inputEventAction, float x, float y) {
				 //on click returns handled value
				 actionHandler.handleAction(action);
				 
			 }
		};
		//add the user input listener (libgdx) to the widget 
		targetActor.addListener(eventListener);
		targetWidget = null; 	//clean the targetWidget,don't wantthis carried over to other widgets targeting th previous targetwidget
	}
	
	
	
	
	
	//handling of easyevent firing attached to each button individuallyF
	private static void addChangeEventHandling (final Actor targetActor) {
	
		if (easyActionType.equals(EasyActionType.VOID)) {
			return;
		}
		
		//copy from set details to final forms of event details for handing
		final EasyActionHandling actionHandler = easyActionHandler;
		final EasyActionType eventType = easyActionType;
		final EasyAction easyAction = new EasyAction(eventType);
		
		easyAction.setTarget(targetWidget);
		 
		//userinput action listener (touch, swipe etc) that comes from libgdx
		EventListener eventListener = new ChangeListener () {
			public void changed(ChangeEvent event, Actor actor) {

				//on click returns handled value
				 actionHandler.handleAction(easyAction);
				 
			}

		};
		//add the user input listener (libgdx) to the widget 
		targetActor.addListener(eventListener);
		targetWidget = null; 	//clean the targetWidget this shouldn't be duplicated targets
	}
	
}
