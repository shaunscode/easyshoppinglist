package com.easy.utils;

import java.util.Comparator;

import com.easy.model.ModelServices;
import com.easy.widgets.CatalogItemWidget;
import com.easy.widgets.ListItemWidget;
import com.easy.widgets.ShopItemWidget;

public class ListItemWidgetComparator implements Comparator<ListItemWidget> {
	
	private boolean groupListItems = false;
	
	public void setGroupListItems (boolean value) {
		groupListItems = value;
	}
	
	@Override
	public int compare(ListItemWidget item, ListItemWidget other) {
		
		if (groupListItems) {
			if (item.isChecked() && !other.isChecked()) {
				return 1;
			} else if (other.isChecked() && !item.isChecked()) {
				return -1;
			} 
		}
		//otherwise neither are checked and can just compare alpha
		return item.getDisplayText().compareToIgnoreCase(other.getDisplayText());
	}
	
}
