package com.easy.utils;

import java.util.Comparator;

import com.easy.model.ModelServices;
import com.easy.widgets.CatalogItemWidget;
import com.easy.widgets.ListItemWidget;
import com.easy.widgets.ShopItemWidget;

public class CatalogItemWidgetComparator implements Comparator<CatalogItemWidget> {
	
	private boolean groupCatalogItems = false;
	
	public void setGroupCatalogItems (boolean value) {
		groupCatalogItems = value;
	}
	
	@Override
	public int compare(CatalogItemWidget item, CatalogItemWidget other) {
		
		//bit of a hack, list widgets need to sort checked items at the botton, while catalog widgets are at the time
		//this just reverses the result ifits a list widget
		if (groupCatalogItems) {
			if (item.isChecked() && !other.isChecked()) {
				return -1;
			} else if (other.isChecked() && !item.isChecked()) {
				return 1;
			}  
		}
		
		
		/*
		 sorting for favourited items, still dunno if this is to beimplements
		if (!(item.isChecked() && other.isChecked())) {
			
			if (item.getFavouriteButton().isChecked() && !other.getFavouriteButton().isChecked()) {
				return -1;
			} else if (other.getFavouriteButton().isChecked() && !item.getFavouriteButton().isChecked()) {
				return 1;
			}	
		}	
		
		 */
		
		//otherwise neither are checked and can just compare alpha
		return item.getDisplayText().compareToIgnoreCase(other.getDisplayText());
	}
	
}
