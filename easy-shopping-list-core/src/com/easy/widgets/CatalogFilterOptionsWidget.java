package com.easy.widgets;

import com.easy.framework.EasyLabelWidget;
import com.easy.model.ModelServices;

public class CatalogFilterOptionsWidget extends EasyLabelWidget {

	public CatalogFilterOptionsWidget () {
		super("Item not found..");
	}

	@Override
	public void update(ModelServices modelServices) {
		getLabel().setText("Item '" + modelServices.getCatalogFilterText() + "' was not found in the catalog...");
	}
	
	

}
