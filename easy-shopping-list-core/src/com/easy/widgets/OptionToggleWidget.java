package com.easy.widgets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.easy.components.EasyActionType;
import com.easy.framework.EasyWidget;
import com.easy.model.Assets;
import com.easy.model.Constants;
import com.easy.model.ModelServices;
import com.easy.utils.UIBuilder;

public class OptionToggleWidget extends EasyWidget {

	private Button button;
	private String id;
	
	public OptionToggleWidget (String id, String displayText, String altText, EasyActionType easyActionType) {
		this.id = id;
		
		float iconSize = Gdx.graphics.getWidth() / 8;
		UIBuilder.setEasyActionType(EasyActionType.VOID);
		UIBuilder.setFont(Assets.FONT_MD);
		UIBuilder.setDisplayText(displayText);
		getTable().add(UIBuilder.getLabel()).align(Align.left).expandX().fillX();
		
		UIBuilder.setButtonDrawables(Assets.ICON_TICK.tint(Constants.COLOR_LIGHT_GREY), Assets.ICON_TICK.tint(Constants.COLOR_LIGHT_GREY), Assets.ICON_TICK.tint(Constants.COLOR_GREEN));
		UIBuilder.setEasyActionType(easyActionType);
		 button = UIBuilder.getImageButton(); 
		getTable().add(button).align(Align.topRight).size(iconSize);
		getTable().row();
		
		UIBuilder.setEasyActionType(EasyActionType.VOID);
		UIBuilder.setFont(Assets.FONT_SM);
		UIBuilder.setDisplayText(altText);
		Label groupListLabel = UIBuilder.getLabel();
		groupListLabel.setWrap(true);
		
		getTable().add(groupListLabel).fillX().expandX().align(Align.left).colspan(2);//.padBottom(Constants.LABEL_PADDING_LEFT);
		
		//put the text size back to normal
		UIBuilder.setFont(Assets.FONT_MD);
		
	}

	public String getId () {
		return this.id;
	}
	
	@Override
	public void update(ModelServices modelServices) {
		button.setChecked(modelServices.isOptionWidgetChecked(this.getId()));
	}
	
}
