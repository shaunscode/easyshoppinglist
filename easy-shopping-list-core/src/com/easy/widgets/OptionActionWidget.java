package com.easy.widgets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;
import com.easy.components.EasyActionType;
import com.easy.framework.EasyWidget;
import com.easy.model.Assets;
import com.easy.model.Constants;
import com.easy.model.ModelServices;
import com.easy.utils.UIBuilder;

public class OptionActionWidget extends EasyWidget {
	
	private Label label;
	
	
	public OptionActionWidget (String displayText, String altText, EasyActionType easyActionType) {
		this(displayText, altText, easyActionType, Assets.ICON_RIGHT_ARROW.tint(Constants.COLOR_GREEN));
	}

	public OptionActionWidget (String displayText, String altText, EasyActionType easyActionType, SpriteDrawable icon) {
		
		
		UIBuilder.setButtonDrawables(icon, icon.tint(Constants.COLOR_LIGHT_GREY), icon);
		
		float iconSize = Gdx.graphics.getWidth() / 8;
		
		UIBuilder.setFont(Assets.FONT_MD);
		UIBuilder.setDisplayText(displayText);
		getTable().add(UIBuilder.getLabel()).align(Align.left).expandX().fillX();
		
		UIBuilder.setEasyActionType(easyActionType);
		getTable().add(UIBuilder.getImageButton()).align(Align.topRight).size(iconSize);
		getTable().row();
		
		UIBuilder.setFont(Assets.FONT_SM);
		UIBuilder.setDisplayText(altText);
		label = UIBuilder.getLabel();
		label.setWrap(true);
		
		getTable().add(label).fillX().expandX().align(Align.left).colspan(2);//.padBottom(Constants.LABEL_PADDING_LEFT);
		
		//put the text size back to normal
		UIBuilder.setFont(Assets.FONT_MD);
	}

	@Override
	public void update(ModelServices modelServices) {
		// TODO Auto-generated method stub
		
	}

	public Label getLabel() {
		return label;
	}

	public void setLabel(Label label) {
		this.label = label;
	}



}
