package com.easy.widgets;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.easy.components.EasyActionType;
import com.easy.framework.EasyAction;
import com.easy.framework.EasyWidget;
import com.easy.model.Assets;
import com.easy.model.Constants;
import com.easy.model.ModelServices;
import com.easy.utils.UIBuilder;

public class CatalogFilterWidget extends EasyWidget {

	private TextField filter;
	private Button clearButton;
	
	public CatalogFilterWidget () {
		
		UIBuilder.setDisplayText("Filter...");
		UIBuilder.setEasyActionType(EasyActionType.CATALOG_FILTER);
		UIBuilder.setTarget(this);
		filter = UIBuilder.getTextField();
		getTable().add(filter).fillX().expandX();

		UIBuilder.setButtonDrawables(Assets.ICON_XCROSS.tint(Constants.COLOR_RED), Assets.ICON_XCROSS.tint(Constants.COLOR_LIGHT_GREY), Assets.ICON_XCROSS.tint(Constants.COLOR_RED));
		UIBuilder.setEasyActionType(EasyActionType.CATALOG_CLEAR_FILTER);
	
		clearButton = UIBuilder.getImageButton();
		getTable().add(clearButton).size(Constants.ICON_SIZE_SM);
		getTable().setBackground(Assets.BG_DIALOG_WIDGET);
		//this.padTop(55);
	}
	
	public String getFilterText () {
		return filter.getText();
	}
	
	
	@Override
	public void update(ModelServices modelServices) {
		
		boolean showing = (Boolean)(modelServices.getCached(Constants.CACHE_CATALOG_FILTER));
		
		if (showing) {
			this.addAction(Actions.show());
		} else {
			this.addAction(Actions.hide());
		}
		
		String filterText = modelServices.getCatalogFilterText(); 

		filter.setText(filterText);
		
		if (filterText.length() == 0) {
			clearButton.addAction(Actions.alpha(0.0f, Constants.FADE_DURATION));	
		} else {
			clearButton.addAction(Actions.alpha(1.0f, Constants.FADE_DURATION));
		}
		
		filter.setCursorPosition(999);	//999 = always at the end
		
	}
	

}
