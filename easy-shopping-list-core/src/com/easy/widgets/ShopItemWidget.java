package com.easy.widgets;

import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.easy.framework.EasyWidget;
import com.easy.model.Assets;
import com.easy.model.ModelServices;
import com.easy.model.ShopItem;
import com.easy.utils.CatalogItemWidgetComparator;

public abstract class ShopItemWidget extends EasyWidget	 {


	private int quantity;
	private String id = "";
	protected TextButton textButton = null;
	
	
	public ShopItemWidget (String displayText, String id) {
		super.setDisplayText(displayText);
		this.id = id;
		this.background(Assets.PANEL);
		initialise();
		
	}
	
	protected abstract void initialise ();
	
	public String getId() {
		return id;
	}
	
	public TextButton getTextButton() {
		return textButton;
	}
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public boolean isChecked() {
		return textButton.isChecked();
	}

	public void setChecked(boolean checked) {
		textButton.setChecked(checked);
	}
	
}
