package com.easy.widgets;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.easy.components.EasyActionType;
import com.easy.framework.EasyDialogWidget;
import com.easy.model.Assets;
import com.easy.model.Constants;
import com.easy.model.ModelServices;
import com.easy.utils.UIBuilder;

public class DeleteWidget extends EasyDialogWidget {
	
	private Label confirmLabel, yesLabel, noLabel;
	private Button yesButton, noButton;

	public DeleteWidget () {
		
		UIBuilder.setFontColor(Constants.COLOR_LABELS);
		
		createConfirmLabel();
		createYesLabel();
		createNoLabel();
		
		createYesButton();
		createNoButton();
		
		getTable().add(confirmLabel).expandX().fillX().align(Align.left);
		getTable().row();
		
		getTable().add(yesLabel).expandX().fillX().padRight(Constants.LABEL_PADDING_RIGHT * 0.5f);			
		getTable().add(yesButton).size(Constants.ICON_SIZE_SM);			
		getTable().row();
		getTable().add(noLabel).expandX().fillX().padRight(Constants.LABEL_PADDING_RIGHT * 0.5f);
		getTable().add(noButton).size(Constants.ICON_SIZE_SM);
		
		
	}
	
	
	
	private void createConfirmLabel () {
		//create text label that does nothing
		UIBuilder.setFont(Assets.FONT_MD);
		UIBuilder.setDisplayText("Delete catalog item?");
		UIBuilder.setEasyActionType(EasyActionType.VOID);
		confirmLabel = UIBuilder.getLabel();
		confirmLabel.setAlignment(Align.left);
		confirmLabel.setWrap(true);		
	}
	
	private void createYesLabel () {
		UIBuilder.setFont(Assets.FONT_MD);
		UIBuilder.setDisplayText("Delete");
		UIBuilder.setEasyActionType(EasyActionType.CATALOG_DELETE_ITEM);
		yesLabel = UIBuilder.getLabel();
		yesLabel.setAlignment(Align.right);
		yesLabel.setWrap(true);		
		
	}
	private void createNoLabel () {
		UIBuilder.setFont(Assets.FONT_MD);
		UIBuilder.setDisplayText("Cancel");
		UIBuilder.setEasyActionType(EasyActionType.CLOSE_DIALOG);
		noLabel = UIBuilder.getLabel();
		noLabel.setAlignment(Align.right);
		noLabel.setWrap(true);		
		
	}
	
	
	private void createYesButton () {
		//add yes button
		UIBuilder.setTarget(this);
		UIBuilder.setEasyActionType(EasyActionType.CATALOG_DELETE_ITEM);
		UIBuilder.setButtonDrawables(Assets.ICON_TICK.tint(Constants.COLOR_GREEN), Assets.ICON_TICK.tint(Constants.COLOR_LIGHT_GREY), Assets.ICON_TICK.tint(Constants.COLOR_GREEN));
		yesButton = UIBuilder.getImageButton();

	}
	
	private void createNoButton () {
		UIBuilder.setTarget(this);
		UIBuilder.setEasyActionType(EasyActionType.CLOSE_DIALOG);
		UIBuilder.setButtonDrawables(Assets.ICON_XCROSS.tint(Constants.COLOR_RED), Assets.ICON_XCROSS.tint(Constants.COLOR_LIGHT_GREY), Assets.ICON_XCROSS.tint(Constants.COLOR_RED));
		noButton = UIBuilder.getImageButton();

	}


	@Override
	public void update(ModelServices modelServices) {
		// TODO Auto-generated method stub
		
	}

}
