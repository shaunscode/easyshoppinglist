package com.easy.widgets;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.easy.components.EasyViewType;
import com.easy.framework.EasyWidget;
import com.easy.model.Assets;
import com.easy.model.Constants;
import com.easy.model.ModelServices;
import com.easy.model.AppModel;
import com.easy.utils.UIBuilder;

public class TitleWidget extends EasyWidget {
	
	public TitleWidget (String displayText) {
		
		//this is a static widget that isn't dependant on anything else. 
		//so build in constructor.

		this.background(Assets.TITLE_PANEL);
		UIBuilder.setFontColor(Color.WHITE);
		UIBuilder.setFont(Assets.FONT_MD);
		UIBuilder.setDisplayText(displayText);
		Label label = UIBuilder.getLabel();
		UIBuilder.setFontColor(Color.BLACK);//put it back to black since this is only place that uses a color other then black
		this.getTable().add(label).expandX().left();	
		UIBuilder.setFont(Assets.FONT_MD);
		
	}

	@Override
	public void update(ModelServices modelServices) {
		
	}

	
}
