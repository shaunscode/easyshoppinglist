package com.easy.widgets;

import com.easy.components.EasyActionType;
import com.easy.model.Assets;
import com.easy.model.Constants;
import com.easy.model.ModelServices;

public class SaveFilterItemWidget extends OptionActionWidget {

	public SaveFilterItemWidget() {
		super("Save item to catalog", "Saves the item to the catalog to then be selected", EasyActionType.CATALOG_SAVE_FILTER_ITEM, Assets.ICON_SAVE.tint(Constants.COLOR_BLUE));
		// TODO Auto-generated constructor stub
	}

	@Override
	public void update(ModelServices modelServices) {
		super.getLabel().setText("Save '" + modelServices.getCatalogFilterText() + "' to the catalog?");
	}
	}
