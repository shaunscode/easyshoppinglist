package com.easy.widgets;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.easy.components.EasyActionType;
import com.easy.model.Assets;
import com.easy.model.Constants;
import com.easy.model.ModelServices;
import com.easy.utils.UIBuilder;

public class CatalogItemWidget extends ShopItemWidget {
	
	private Container optionButtonsContainer;
	private Table optionButtonsTable;
	
	private Container confirmDeleteContainer;
	private Table confirmDeleteTable;
	
	private Label confirmLabel;
	
	private Button 
		optionsOpenButton,
		//optionsCloseButton,
		deleteButton,
		favButton,
		minusButton,
		plusButton,
		yesButton,
		noButton;
	
	
	public CatalogItemWidget (String displayText, String id) {
		super(displayText, id);
		//addButtons();
	}
	
	public void initialise() {		
		
		createButtons();
		addDisplayTextButton();
		addOptionsContainer();
		addDeleteContainer();
		
	}
	
	private void createButtons () {
		
		createOptionsButtons();
		createDeleteButton();
		createDeleteLabel();
		createFavButton();
		createMinusButton();
		createPlusButton();
		createYesButton();
		createNoButton();
		createDisplayTextButton();
	}
	
	
	
	private void createOptionsButtons () {
		UIBuilder.setTarget(this);
		UIBuilder.setEasyActionType(EasyActionType.CATALOG_OPTIONS_TOGGLE);
		UIBuilder.setButtonDrawables(Assets.ICON_VERTICAL_DOTS.tint(Constants.COLOR_LIGHT_GREY), Assets.ICON_VERTICAL_DOTS.tint(Constants.COLOR_LIGHT_GREY), Assets.ICON_VERTICAL_DOTS.tint(Constants.COLOR_GREEN));
		optionsOpenButton = UIBuilder.getImageButton();
		
		//UIBuilder.setTarget(this);
		//UIBuilder.setEasyActionType(EasyActionType.CATALOG_OPTIONS_TOGGLE);
		//UIBuilder.setButtonDrawables(Assets.ICON_UP_ARROW.tint(Constants.COLOR_GREEN), Assets.ICON_UP_ARROW.tint(Constants.COLOR_LIGHT_GREY), Assets.ICON_UP_ARROW.tint(Constants.COLOR_GREEN));
		//optionsCloseButton = UIBuilder.getImageButton();
		
	}
	
	private void createDeleteButton () {
		//delete button that toggles the delete confirm popup table container
		UIBuilder.setTarget(this);
		UIBuilder.setEasyActionType(EasyActionType.SHOW_DELETE_DIALOG);
		UIBuilder.setButtonDrawables(Assets.ICON_TRASHCAN.tint(Constants.COLOR_ORANGE), Assets.ICON_TRASHCAN.tint(Constants.COLOR_ORANGE), Assets.ICON_TRASHCAN.tint(Constants.COLOR_ORANGE));
		deleteButton = UIBuilder.getImageButton();

	}
	
	private void createFavButton () {
		//fave button 
		UIBuilder.setTarget(this);
		UIBuilder.setEasyActionType(EasyActionType.CATALOG_FAVOURITE_TOGGLE);
		UIBuilder.setButtonDrawables(Assets.ICON_STAR.tint(Constants.COLOR_LIGHT_GREY), Assets.ICON_STAR.tint(Constants.COLOR_LIGHT_GREY), Assets.ICON_STAR.tint(Constants.COLOR_YELLOW));
		favButton = UIBuilder.getImageButton();

	}
	
	private void createMinusButton () {
		//minu quantity button
		UIBuilder.setTarget(this);
		UIBuilder.setEasyActionType(EasyActionType.CATALOG_MINUS_QUANTITY);
		UIBuilder.setButtonDrawables(Assets.ICON_MINUS.tint(Constants.COLOR_RED), Assets.ICON_MINUS.tint(Constants.COLOR_LIGHT_GREY), Assets.ICON_MINUS.tint(Constants.COLOR_RED));
		minusButton = UIBuilder.getImageButton();
				
	}
	
	private void createPlusButton () {
		//add quantity button
		UIBuilder.setTarget(this);
		UIBuilder.setEasyActionType(EasyActionType.CATALOG_PLUS_QUANTITY);
		UIBuilder.setButtonDrawables(Assets.ICON_PLUS.tint(Constants.COLOR_GREEN), Assets.ICON_PLUS.tint(Constants.COLOR_LIGHT_GREY), Assets.ICON_PLUS.tint(Constants.COLOR_GREEN));
		plusButton = UIBuilder.getImageButton();

	}
	
	private void createYesButton () {
		//add yes button
		UIBuilder.setTarget(this);
		UIBuilder.setEasyActionType(EasyActionType.CATALOG_DELETE_ITEM);
		UIBuilder.setButtonDrawables(Assets.ICON_Y.tint(Constants.COLOR_GREEN), Assets.ICON_Y.tint(Constants.COLOR_LIGHT_GREY), Assets.ICON_Y.tint(Constants.COLOR_GREEN));
		yesButton = UIBuilder.getImageButton();

	}
	
	private void createNoButton () {
		UIBuilder.setTarget(this);
		UIBuilder.setEasyActionType(EasyActionType.SHOW_DELETE_DIALOG);
		UIBuilder.setButtonDrawables(Assets.ICON_N.tint(Constants.COLOR_RED), Assets.ICON_N.tint(Constants.COLOR_LIGHT_GREY), Assets.ICON_N.tint(Constants.COLOR_RED));
		noButton = UIBuilder.getImageButton();

	}
	
	private void createDisplayTextButton () {
		
		UIBuilder.setFontColor(Constants.COLOR_LABELS);
		UIBuilder.setCheckedFontColor(Constants.COLOR_ORANGE);
		UIBuilder.setDownFontColor(Constants.COLOR_ORANGE);
		UIBuilder.setDisplayText(super.getDisplayText());
		UIBuilder.setEasyActionType(EasyActionType.CATALOG_TOGGLE_ITEM);
		UIBuilder.setTarget(this);
		textButton = UIBuilder.getTextButton();
		textButton.getLabel().setAlignment(Align.left);
		textButton.getLabel().setWrap(true);
	}
	
	private void createDeleteLabel () {
		//create text label that does nothing
		UIBuilder.setFont(Assets.FONT_MD);
		UIBuilder.setDisplayText("Delete?");
		UIBuilder.setEasyActionType(EasyActionType.VOID);
		confirmLabel = UIBuilder.getLabel();
		confirmLabel.setAlignment(Align.center);
		confirmLabel.setWrap(true);		
	}
	
	private void addDisplayTextButton () {
		//add to the widget's table
		this.getTable().add(textButton).fillX().expandX();
		getTable().add(optionsOpenButton).size(Constants.ICON_SIZE_SM);
	}
	
	private void addOptionsContainer () {

		optionButtonsContainer = new Container();		
		optionButtonsTable = new Table();		
		
		optionButtonsTable.add(deleteButton).size(Constants.ICON_SIZE_SM).expandX();		
		//optionButtonsTable.add(favButton).size(Constants.ICON_SIZE_SM).expandX();		
		optionButtonsTable.add(minusButton).size(Constants.ICON_SIZE_SM).expandX();		
		optionButtonsTable.add(plusButton).size(Constants.ICON_SIZE_SM).expandX();
		//optionButtonsTable.add(optionsCloseButton).size(Constants.ICON_SIZE_SM).expandX();
		
		//create the container and add to widget table
		optionButtonsContainer.fill();
	
		//add the (empty) container to widget
		getTable().row();
		getTable().add(optionButtonsContainer).expandX().fillX().colspan(2);
	
	}
	
	private void addDeleteContainer () {
		
		//create and add the container to the widget table
		confirmDeleteContainer = new Container();
		confirmDeleteContainer.fill();		
				
		//create a table to hold the confirm delete text/icons
		confirmDeleteTable = new Table();
		confirmDeleteTable.add(confirmLabel).expandX().fillX().colspan(2).padTop(15.0f);
		confirmDeleteTable.row();
		
		confirmDeleteTable.add(noButton).size(Constants.ICON_SIZE_LG);
		confirmDeleteTable.add(yesButton).size(Constants.ICON_SIZE_LG);			
		
		getTable().row();
		getTable().add(confirmDeleteContainer).expandX().fillX().colspan(2);
		
		
	}

	
	@Override
	public void update (ModelServices modelServices) {		
		
		updateDisplayTextButtons (modelServices);
		updateOptionsContainer (modelServices);
		updateDeleteContainer (modelServices);
		updateDisplayQuantity (modelServices);
		
	}
	
	private void updateDisplayTextButtons (ModelServices modelServices) {
		
		boolean checked = modelServices.isShopItemCheckedCatalog(getId());
		boolean isOptionsShowing = modelServices.isShopItemShowingOptionsContainer(getId());
		
		setChecked(checked);	//just updates this widgets button state, regardless true/false
		if (checked) { // && !isOptionsShowing) {
			optionsOpenButton.addAction(Actions.alpha(1.0f));
			optionsOpenButton.setDisabled(false);
		} else {
			optionsOpenButton.addAction(Actions.alpha(0.0f));
			optionsOpenButton.setDisabled(true);
		}
		
		
	}
	
	private void updateOptionsContainer (ModelServices modelServices) {
		
		boolean showOptions = modelServices.isShopItemShowingOptionsContainer(getId());
		
		optionsOpenButton.setChecked(showOptions);
		if (showOptions) {
			optionButtonsContainer.setActor(optionButtonsTable);
			optionButtonsContainer.padTop(15.0f);
		} else {
			optionButtonsContainer.padTop(0.0f);
			optionButtonsContainer.setActor(null);
			confirmDeleteContainer.setActor(null);		//can't be showing delete if options aren't showing
		}
		
	}
	
	private void updateDeleteContainer (ModelServices modelServices) {
		//if the delete button has been toggled, add the table to the container
		boolean showDelete = modelServices.isShopItemShowingDeleteContainer(getId());
		
		deleteButton.setChecked(showDelete);
		if (showDelete) {
			confirmDeleteContainer.setActor(confirmDeleteTable);
		} else {
			confirmDeleteContainer.setActor(null);
		}
	}
	
	private void updateDisplayQuantity (ModelServices modelServices) {
		int quantity = modelServices.getShopItemQuantity(getId());
		
		//set to default display text
		String displayText = modelServices.getShopItemDisplayText(getId());
		textButton.getLabel().setText(displayText);
		if (quantity > 1) {
			textButton.getLabel().setText(quantity + " x " + displayText);
			minusButton.setDisabled(false);
		} else {
			minusButton.setDisabled(true);
		}
	}

	public Button getFavouriteButton() {
		return favButton;
	}
	public Button getDeleteButton() {
		return deleteButton;
	}

	public Button getMinusButton() {
		return minusButton;
	}

	public Button getPlusButton() {
		return plusButton;
	}

	public Button getYesButton() {
		return yesButton;
	}

	public Button getNoButton() {
		return noButton;
	}

}
