package com.easy.widgets;

import java.util.Comparator;

import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Align;
import com.easy.components.EasyActionType;
import com.easy.framework.EasyWidget;
import com.easy.model.Constants;
import com.easy.model.ModelServices;
import com.easy.model.AppModel;
import com.easy.model.Assets;
import com.easy.model.ShopItem;
import com.easy.utils.UIBuilder;

public class ListItemWidget extends ShopItemWidget {

	public ListItemWidget (String displayText, String id) {
		super(displayText, id);
	}
	
	public void initialise () {
		UIBuilder.setFontColor(Constants.COLOR_LABELS);
		UIBuilder.setCheckedFontColor(Constants.COLOR_LIGHT_GREY);
		UIBuilder.setDownFontColor(Constants.COLOR_LIGHT_GREY);
		UIBuilder.setDisplayText(super.getDisplayText());
		UIBuilder.setEasyActionType(EasyActionType.LIST_TOGGLE_ITEM);
		UIBuilder.setTarget(this);
		
		textButton = UIBuilder.getTextButton();
		textButton.getLabel().setAlignment(Align.left);
		textButton.getLabel().setWrap(true);
		this.getTable().add(textButton).fillX().expandX();
		
	}	
	
	@Override
	public void update(ModelServices modelServices) {
		setChecked(modelServices.isShopItemCheckedList(getId()));
		
		
		String displayText = modelServices.getShopItemDisplayText(getId());
		int quantity = modelServices.getShopItemQuantity(getId());
		//set to default display text
		textButton.getLabel().setText(displayText);
		if (quantity > 1) {
			textButton.getLabel().setText(quantity + " x " + displayText);
		}
		
	}
}
