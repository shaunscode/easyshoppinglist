package com.easy.widgets;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.easy.components.EasyActionType;
import com.easy.framework.EasyWidget;
import com.easy.model.Assets;
import com.easy.model.Constants;
import com.easy.model.ModelServices;
import com.easy.utils.UIBuilder;
import com.easy.views.MenuViews;

public class MenuWidget extends EasyWidget {
	
	
	private Button menuCatalogButton, menuListButton, menuCalcButton, menuOptionsButton;
	private ButtonGroup<Button> bg;
	
	public MenuWidget () {
	

		UIBuilder.setButtonDrawables(Assets.ICON_CATALOG, Assets.ICON_CATALOG, Assets.ICON_CATALOG_CHECKED);
		UIBuilder.setEasyActionType(EasyActionType.SHOW_MENU_CATALOG);
		menuCatalogButton = UIBuilder.getImageButton();
		//addEasyEventListener (menuCatalogButton, this, easyActionType.MENU_CATALOG);
		
		
		UIBuilder.setButtonDrawables(Assets.ICON_LIST, Assets.ICON_LIST, Assets.ICON_LIST_CHECKED);
		UIBuilder.setEasyActionType(EasyActionType.SHOW_MENU_LIST);
		menuListButton = UIBuilder.getImageButton();
		
		UIBuilder.setButtonDrawables(Assets.ICON_CALC, Assets.ICON_CALC, Assets.ICON_CALC_CHECKED);
		UIBuilder.setEasyActionType(EasyActionType.SHOW_MENU_CALC);
		menuCalcButton = UIBuilder.getImageButton();
		
		UIBuilder.setButtonDrawables(Assets.ICON_OPTIONS, Assets.ICON_OPTIONS, Assets.ICON_OPTIONS_CHECKED);
		UIBuilder.setEasyActionType(EasyActionType.SHOW_MENU_OPTIONS);
		menuOptionsButton = UIBuilder.getImageButton();
		
		bg = new ButtonGroup<Button>(menuCatalogButton, menuListButton, menuCalcButton, menuOptionsButton);
		bg.setMaxCheckCount(1);
		bg.setMinCheckCount(1);
		bg.setUncheckLast(true);
		
	
		
		this.getTable().add(menuCatalogButton).size(Constants.ICON_SIZE_LG).expandX();
		this.getTable().add(menuListButton).size(Constants.ICON_SIZE_LG).expandX();
		this.getTable().add(menuCalcButton).size(Constants.ICON_SIZE_LG).expandX();
		this.getTable().add(menuOptionsButton).size(Constants.ICON_SIZE_LG).expandX();
		this.getTable().padLeft(Constants.LABEL_PADDING_LEFT / 2);
		this.getTable().padRight(Constants.LABEL_PADDING_RIGHT / 2);
		
		
	//	getTable().padTop(100);
	//	getTable().background(Assets.TOP_BORDER_ONLY);
	}

	@Override
	public void update(ModelServices modelServices) {

		bg.uncheckAll();
		Object cachedSelectedMenu = modelServices.getCached(Constants.CACHE_SELECTED_MENU);
		MenuViews selectedMenu = (MenuViews)cachedSelectedMenu;
		
		switch(selectedMenu) {
		
		case CATALOG:
			menuCatalogButton.setChecked(true);
			break;
		case LIST:
			menuListButton.setChecked(true);
			break;
		case CALC:
			menuCalcButton.setChecked(true);
			break;
		case OPTIONS:
			menuOptionsButton.setChecked(true);
			break;
		}
	}
	
}
