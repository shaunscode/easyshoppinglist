package com.easy.controllers;

import com.easy.framework.EasyAction;
import com.easy.framework.EasyController;
import com.easy.framework.EasyDialogView;
import com.easy.model.Constants;
import com.easy.views.MenuViews;
import com.easy.widgets.DeleteWidget;

public class OverlayViewController extends EasyController {

	@Override
	public void handleAction (EasyAction event) {		
		 
		switch (event.getEasyActionType()) {
		
		
		
		case SHOW_MENU_CATALOG:
			getModelServices().setCached(Constants.CACHE_SELECTED_MENU, MenuViews.CATALOG);
			break;
		case SHOW_MENU_LIST:
			getModelServices().setCached(Constants.CACHE_SELECTED_MENU, MenuViews.LIST);
			break;
		case SHOW_MENU_CALC:
			getModelServices().setCached(Constants.CACHE_SELECTED_MENU, MenuViews.CALC);
			break;
		case SHOW_MENU_OPTIONS:
			getModelServices().setCached(Constants.CACHE_SELECTED_MENU, MenuViews.OPTIONS);
			break;
		}
	}
	
}
