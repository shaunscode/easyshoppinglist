package com.easy.controllers;

import com.easy.framework.EasyView;
import com.easy.components.EasyActionType;
import com.easy.framework.EasyAction;
import com.easy.framework.EasyController;
import com.easy.model.Constants;

public class OptionsViewController extends EasyController {


	@Override
	public void handleAction(EasyAction event) {
		boolean currentState;
		
		switch (event.getEasyActionType()) {
		case OPTIONS_TOGGLE_GROUP_LIST_ITEMS:
			currentState = getModelServices().isOptionWidgetChecked(Constants.OPTION_GROUP_LIST);
			getModelServices().setOptionWidgetChecked(Constants.OPTION_GROUP_LIST, !currentState);
			break;
			
		case OPTIONS_TOGGLE_GROUP_CATALOG_ITEMS:
			currentState = getModelServices().isOptionWidgetChecked(Constants.OPTION_GROUP_CATALOG);
			getModelServices().setOptionWidgetChecked(Constants.OPTION_GROUP_CATALOG, !currentState);
			break;
			
		case UPDATE_OPTIONS_VIEW:
			
			break;
			
		default:
			return;	
		}
		
	}

	
	
}
