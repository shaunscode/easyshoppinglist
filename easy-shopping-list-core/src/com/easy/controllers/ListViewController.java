package com.easy.controllers;

import com.easy.framework.EasyView;
import com.easy.components.EasyActionType;
import com.easy.framework.EasyAction;
import com.easy.framework.EasyController;
import com.easy.widgets.CatalogItemWidget;
import com.easy.widgets.ListItemWidget;

public class ListViewController extends EasyController {


	@Override
	public void handleAction(EasyAction event) {
		
		
		switch (event.getEasyActionType()) {
		case LIST_TOGGLE_ITEM:
			ListItemWidget widget = (ListItemWidget)event.getTarget();
			
			boolean currentCheckState = getModelServices().isShopItemCheckedList(widget.getId());
			boolean isChecked = !currentCheckState;
			getModelServices().setShopItemCheckedList(widget.getId(), isChecked);
			
			break;
					
		case UPDATE_LIST_VIEW:
			break;
			

		default:
			return;
		}
		
	}


	
}
