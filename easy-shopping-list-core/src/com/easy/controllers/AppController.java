package com.easy.controllers;

import com.badlogic.gdx.utils.Array;
import com.easy.framework.EasyAction;
import com.easy.framework.EasyController;

public class AppController extends EasyController {
	
	
	private Array<EasyController> subControllers = new Array<EasyController>();
	
	public void addController (EasyController controller) {
		subControllers.add(controller); 
	}
	
	
	@Override
	public void handleAction(EasyAction action) {
		
		System.out.println("AppController received action: " + action.getEasyActionType());

		for (EasyController controller: subControllers) {		
			controller.handleAction(action);
		}
		
		updateView();
		
		return;
	}


}
