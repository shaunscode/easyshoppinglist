package com.easy.controllers;

import com.easy.framework.EasyAction;
import com.easy.framework.EasyController;
import com.easy.framework.EasyDialogWidget;
import com.easy.framework.EasyPopupsView;
import com.easy.framework.EasyWidget;
import com.easy.model.Constants;
import com.easy.widgets.CatalogItemWidget;
import com.easy.widgets.DeleteWidget;

public class DialogController extends EasyController {

	@Override
	public void handleAction(EasyAction action) {

		switch (action.getEasyActionType()) {
		case CLOSE_DIALOG:
			getModelServices().setCached(Constants.CACHE_DIALOG, null);
			break;
			
		case SHOW_DELETE_DIALOG:
			DeleteWidget deleteWidget = new DeleteWidget();
			deleteWidget.setTargetWidget((EasyWidget)action.getTarget());
			getModelServices().setCached(Constants.CACHE_DIALOG, deleteWidget);
			
			//boolean isShowingDelete = getModelServices().isShopItemShowingDeleteContainer(widget.getId());
			//getModelServices().setShopItemShowingDeleteContainer(widget.getId(), !isShowingDelete);
			break;
		case CATALOG_DELETE_ITEM: 
			
			EasyDialogWidget dialogWidget = (EasyDialogWidget)getModelServices().getCached(Constants.CACHE_DIALOG);
			CatalogItemWidget widgetToDelete = (CatalogItemWidget)dialogWidget.getTargetWidget();
			getModelServices().removeShopItem(widgetToDelete.getId());
			EasyPopupsView.post(widgetToDelete.getDisplayText() + " has been deleted from the catalog");
			//cleanup dialog of screen
			getModelServices().setCached(Constants.CACHE_DIALOG, null);
			break;
		
		}
	}
}
