package com.easy.controllers;

import com.easy.framework.EasyAction;
import com.easy.framework.EasyController;
import com.easy.views.CatalogView;
import com.easy.widgets.CatalogFilterWidget;

public class CatalogViewController  extends EasyController {


	@Override
	public void handleAction(EasyAction event) {
		
		switch (event.getEasyActionType()) {
		
		case CATALOG_FILTER:
			
			String filterText = ((CatalogFilterWidget)event.getTarget()).getFilterText();
			getModelServices().setCatalogFilterText(filterText);
			
			break;
				
		case UPDATE_CATALOG_VIEW:
			break;
			
			
		case CATALOG_SAVE_FILTER_ITEM:
			getModelServices().addShopItem(getModelServices().getCatalogFilterText(), 0, false, false, false);

			break;
			
		case CATALOG_CLEAR_FILTER: 
			getModelServices().setCatalogFilterText("");
			
			break;
			
		default:
			return;
		}
		
	}

	
	
}
