package com.easy.controllers;

import com.easy.framework.EasyAction;
import com.easy.framework.EasyController;
import com.easy.framework.EasyPopupsView;
import com.easy.model.Constants;
import com.easy.widgets.CatalogItemWidget;
import com.easy.widgets.DeleteWidget;

public class CatalogItemsViewController extends EasyController {

	@Override
	public void handleAction(EasyAction event) {
		
		CatalogItemWidget widget = null;
		
		switch (event.getEasyActionType()) {
		case CATALOG_SHOW_FILTER:
			getModelServices().setCached(Constants.CACHE_CATALOG_FILTER, new Boolean(false));
			break;
		case CATALOG_HIDE_FILTER:
			getModelServices().setCached(Constants.CACHE_CATALOG_FILTER, new Boolean(true));
			break;
		
		 
		
		case CATALOG_PLUS_QUANTITY:
			widget = (CatalogItemWidget)event.getTarget();
			
			getModelServices().incrementShopItemQuantity(widget.getId());
			getModelServices().setShopItemShowingDeleteContainer(widget.getId(), false);
			break;
			
		case CATALOG_MINUS_QUANTITY:
			widget = (CatalogItemWidget)event.getTarget();
		
			if (getModelServices().isShopItemCheckedCatalog(widget.getId()) && getModelServices().getShopItemQuantity(widget.getId()) < 2) {
				return;
			}
			
			getModelServices().decrementShopItemQuantity(widget.getId());
			getModelServices().setShopItemShowingDeleteContainer(widget.getId(), false);
			
			break;
			
			
			
		case CATALOG_TOGGLE_ITEM:
			widget = (CatalogItemWidget)event.getTarget();

			boolean currentCheckState = getModelServices().isShopItemCheckedCatalog(widget.getId());		
			boolean isChecked = !currentCheckState;
			getModelServices().setShopItemCheckedCatalog(widget.getId(), isChecked);
			
			//taken off list, so uncheck it 
			if (!isChecked) {
				getModelServices().setShopItemCheckedList(widget.getId(), false);
				getModelServices().setShopItemQuantity(widget.getId(), 0);
				getModelServices().setShopItemShowingDeleteContainer(widget.getId(), false);
				getModelServices().setShopItemShowingOptionsContainer(widget.getId(), false);
			} else {
				getModelServices().setShopItemQuantity(widget.getId(), 1);
			}
			break;
			
		case CATALOG_OPTIONS_TOGGLE:
			widget = (CatalogItemWidget)event.getTarget();
			
			
			if (!getModelServices().isShopItemCheckedCatalog(widget.getId())) {
				return;
			}
	
			//get new state before closing all (will close the one currently in the process of opening/closing anyhow)
			boolean currentShowingOptions = getModelServices().isShopItemShowingOptionsContainer(widget.getId());
			boolean isShowingOptions = !currentShowingOptions;	
			
			//close all widgets that have options open
			for (String key: getModelServices().getShopItems().keySet()) {				
				getModelServices().getShopItems().get(key).setShowingOptionsContainer(false);
				getModelServices().getShopItems().get(key).setShowingDeleteContainer(false);
			}
			
			//now set the current one to its new state - at this point it will be closed - so close, or open, regardless.
			getModelServices().setShopItemShowingOptionsContainer(widget.getId(), isShowingOptions);
						
			if (!isShowingOptions) {
				
				getModelServices().setShopItemShowingDeleteContainer(widget.getId(), false);
			} 
			break;
			
			
		case CATALOG_FAVOURITE_TOGGLE:
			widget = (CatalogItemWidget)event.getTarget();
			
			boolean currentFavourite = getModelServices().isShopItemFavourite(widget.getId());
			boolean isFavourite = !currentFavourite;
			getModelServices().setShopItemShowingOptionsContainer(widget.getId(), isFavourite);
			
			break;
			
		default:
			return;
		}
		
	}

	
	
}
