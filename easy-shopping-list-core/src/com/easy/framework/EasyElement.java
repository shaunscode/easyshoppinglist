package com.easy.framework;

import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.easy.components.EasyUpdating;
import com.easy.model.ModelServices;

public abstract class EasyElement extends Container implements EasyUpdating {
	
	private EasyLayers layers = new EasyLayers();
	
	
	public EasyElement () {
		this.setActor(layers);
		this.fill();
		
		
	}
	
	protected EasyLayers getLayers() {
		return layers;
	}
	
	public ScrollPane scrollable () {
		return layers.scrollable();
	}
	
	protected Table getTable () {
		return layers.getTable();
	}
	
	public void update (ModelServices modelServices) {
		layers.update(modelServices);
	}
	
	protected EasyLayer layer () {
		EasyLayer layer = layers.layer();
		return layer;
	}
	

	protected <T extends EasyElement> Cell<T> addElement (T element) {
		if (getTable().getChildren().size > 0) {
			getTable().row();
		}
		return getTable().add(element).expandX().fillX().top();
		
	}


}
