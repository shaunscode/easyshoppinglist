package com.easy.framework;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.easy.components.EasyUpdating;
import com.easy.model.ModelServices;

public class EasyLayers extends Stack implements EasyUpdating {

	private EasyLayer layer;
	
	
	public EasyLayers () {
		layer = new EasyLayer();
		this.add(layer);
		
	}
	
	public ScrollPane scrollable () {
		return layer.scrollable();
	}
	
	

	public EasyLayer layer() {
		layer = new EasyLayer();
		this.add(layer);
		
		
		//System.out.println("layers children: " + this.getChildren().size);
		return layer;
	}

	public Table getTable () {
		return layer.getTable();
	}

	
	@Override
	public void update(ModelServices modelServices) {
		for (Actor layer: this.getChildren()) {
			((EasyUpdating)layer).update(modelServices);
		}
	}
	
}
