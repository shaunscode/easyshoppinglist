package com.easy.framework;

import com.easy.components.EasyActionType;

public class EasyAction {

	private EasyActionType easyActionType;
	private StringBuilder eventMessages = new StringBuilder();
	public int passed = 0;
	private EasyElement target;
	public void setTarget(EasyElement target) {
		this.target = target;
	}

	public EasyElement getTarget() {
		return target;
	}

	private Object[] args;

	public EasyAction (EasyActionType inputEventType, Object... args) {
		this.easyActionType = inputEventType;
		this.target = target;
		this.args = args;
	}

	public Object[] getArgs () {
		return this.args;
	}
	
	public EasyActionType getEasyActionType() {
		return easyActionType;
	}
	
	public void appendMessage (String msg) {
		if (eventMessages.length() == 0) {
			eventMessages.append("\n\n### ");
		}
		eventMessages.append(msg);
	}
	
	public String getMessages () {
		String handledByString = this.eventMessages.toString();
		eventMessages.delete(0, eventMessages.length());
		return handledByString;
	}
	
	
	public int getPassed () {
		return this.passed;
	}
	
	public void resetPassed () {
		this.passed = 0;
	}
	
	public String getPassedDashes () {
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < passed; i++) {
			str.append("- ");
		}
		return str.toString();
	}
}
