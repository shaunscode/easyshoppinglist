package com.easy.framework;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.easy.components.EasyUpdating;
import com.easy.model.ModelServices;

public class EasyLayer extends Container implements EasyUpdating {

	private Table table = new Table();
	
	public EasyLayer () {
		 
		this.setActor(table);
		getTable().top();
		this.fill();

	}
	
	
	
	private EasyLayer (Table table) {
		super(table);
		this.table = table;
		getTable().top();
	}
	
	public ScrollPane scrollable () {
		ScrollPane scrollPane = new ScrollPane(getTable());
		scrollPane.setScrollingDisabled(true, false);
		this.setActor(scrollPane);
		this.fill();
		return scrollPane;
	}
	
	public Table getTable () {
		return table;
	}
	
	@Override
	public void update(ModelServices modelServices) {
		for (Actor a: getTable().getChildren()) {
			((EasyUpdating)a).update(modelServices);
		}
	}
	
	
	public String toString () {
		return super.toString() + " (Container)";
	}
}
