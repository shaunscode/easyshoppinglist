package com.easy.framework;

import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.easy.model.AppModel;
import com.easy.model.ModelServices;
import com.easy.utils.UIBuilder;

public abstract class EasyView extends EasyElement {
	
	public EasyView () {
		this(false);
	}
	
	public EasyView (boolean scrolls) {
		
		if (scrolls) { 
			scrollable();
		}
		getTable().top();
		fill();		

	}
	
	
	public Cell<EasyView> addView (EasyView view) {
		return addElement(view);
	}
		
	public Cell<EasyWidget> addWidget (EasyWidget widget) {
		return addElement(widget);
	}
	
	
}
