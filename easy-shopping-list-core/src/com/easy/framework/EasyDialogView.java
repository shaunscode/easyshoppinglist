package com.easy.framework;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Array;
import com.easy.model.Assets;
import com.easy.model.Constants;
import com.easy.model.ModelServices;

public class EasyDialogView extends EasyView {

private static Array<EasyDialogWidget> widgets = new Array<EasyDialogWidget>();
	
	
	public EasyDialogView () {
		setFillParent(true);
	}
	

	public static void post (EasyDialogWidget widget) {
		widgets.add(widget);
	}
	
	@Override
	public void update (ModelServices modelServices) {
		EasyDialogWidget widget = (EasyDialogWidget)modelServices.getCached(Constants.CACHE_DIALOG);
		if (widget == null) {
			this.setActor(null);
			this.setBackground(Assets.BG_SOLID_WHITE.tint(new Color(1.0f, 1.0f, 1.0f, 0.0f)));
		} else {
			setActor(widget);
			this.setBackground(Assets.BG_SOLID_WHITE.tint(new Color(0.95f, 0.95f, 0.95f, 0.8f)));
		}
		
		
		/*
		if (this.getActor() == null) {
			if (widgets.size > 0) {
				EasyDialogWidget nextWidget = widgets.get(0);
				widgets.removeIndex(0);
				setActor(nextWidget);
				this.setBackground(Assets.BG_SOLID_WHITE.tint(new Color(1.0f, 1.0f, 1.0f, 0.6f)));
			} else {
				this.setBackground(Assets.BG_SOLID_WHITE.tint(new Color(1.0f, 1.0f, 1.0f, 0.0f)));
			}		
		} 
		*/
	}
}
