package com.easy.framework;

import com.badlogic.gdx.utils.Array;
import com.easy.model.ModelServices;

public class EasyConsoleView extends EasyView {

	
	Array<ConsoleMessage> msgs = new Array<ConsoleMessage>();
	
	public static void echo (String message) {
		
	}
	
	@Override 
	public void update (ModelServices modelServices) {
		
	}
	
	
	private static class ConsoleMessage implements Comparable<ConsoleMessage> {
		
		String msg;
		long time;
		public ConsoleMessage (String msg) {
			this.msg = msg;
			this.time = System.currentTimeMillis();
		}
		@Override
		public int compareTo(ConsoleMessage other) {
			if (this.time < other.time) {
				return -1;
			}
			return 1;
		}
	}
}
