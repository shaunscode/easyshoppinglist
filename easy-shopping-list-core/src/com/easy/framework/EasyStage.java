package com.easy.framework;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.easy.components.EasyUpdating;
import com.easy.model.Constants;
import com.easy.model.ModelServices;

public class EasyStage implements EasyUpdating {
	
	private Stage stage = new Stage(Constants.VIEW_PORT);
	
	public EasyStage (EasyView rootView) {
		stage.addActor(rootView);
		rootView.setFillParent(true);
	}

	public InputProcessor getInputProcessor () {
		return stage;
	}
	
	public void refresh () {
		stage.act();
		stage.draw();
	}
	
	public void update (ModelServices modelServices) {
	//	consoleView.update(modelServices);
		//buttonsView.update(modelServices);
		for (Actor a: stage.getActors()) {
			((EasyUpdating)a).update(modelServices);
		}
		
	}
	
	

}
