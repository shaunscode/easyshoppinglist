package com.easy.framework;

import java.util.HashMap;

import com.easy.model.ModelServices;

public abstract class EasyMultiView extends EasyView {

	
	private HashMap<String, EasyView> views = new HashMap<String, EasyView>();
	
	public EasyMultiView () {
		this.fill();
	}
	
	public void addView (EasyView view, String viewName) {
		if (views.containsKey(viewName)) {
			System.out.println("MultiView: views map already contains key '" + viewName + "' and does not allow dupliates.");
			return;
		}
		views.put(viewName, view);
		
		//TODO: Why is this here? Breaks without it
		addView(view);
	}
	
	public boolean setView (String viewName) {
		if (!views.containsKey(viewName)) {
			System.out.println("MulitView.setView(); cannot locate viewName '" + viewName + "' in available views ap");
			return false;
		}
		this.setActor(views.get(viewName));
		return true;
	}
	
	public String[] getViewNames () {
		String[] viewNames = new String[views.size()];
		return views.keySet().toArray(viewNames);
		
	}

	protected HashMap<String, EasyView> getViews () {
		return views;
	}

	@Override
	public void update (ModelServices model) {
		for (String viewName: views.keySet()) {
			views.get(viewName).update(model);
		}
	}
	
}
