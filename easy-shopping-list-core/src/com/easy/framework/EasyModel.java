package com.easy.framework;

import com.badlogic.gdx.Preferences;
import com.easy.model.Constants;

public abstract class EasyModel {

	protected Preferences saveData;
	protected String saveName;
	
	public EasyModel (Preferences saveData, String saveName) {
		
		this.saveData = saveData;
		this.saveName = saveName;
		
		loadModelData();
		populateModel();
		
		
	}
	
	private void loadModelData () {
		
		init();
		
		String modelDataString = saveData.getString(saveName);
		float version = saveData.getFloat("EasyShoppingListVersion");
		
		if (modelDataString.length() == 0 || version != Constants.VERSION) {
			modelDataString = getDefaultModelDataString();
			saveData.putString(saveName, modelDataString);
			saveData.flush();	//TODO: Look into why this has to be here. the modelDataString gets put into the saveData, then on return of thi smethod getsextracted from saveData again, though it doesn't seem to save it unless it's flushed.
		}
		
	}
	
 	public void saveModelData () {
 		
 		saveData.putFloat("EasyShoppingListVersion", Constants.VERSION);
 		saveData.putString(saveName, generateSaveDataString());
 		saveData.flush();
		
 	}
	
 	//load model vars here - below methodsget called gtom this constructor and may use vars that have not been set yet
	protected abstract void init ();
	
	//when auto load of a string from the saveData is empty, populate it with a default set of into. this info will then be saved by the calling method above
 	protected abstract String getDefaultModelDataString ();
 	
 	//turn whatever the model data is for into a string for saving
 	protected abstract String generateSaveDataString ();
 	
 	//when you have the model data string from the Preferences, turn it into objs etc in the model for use
 	//eg. saveData holds a string, in this moethod, pull the string from the saveData, and manipulate it into objs/values needed.
	protected abstract void populateModel ();
}
