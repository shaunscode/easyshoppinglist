package com.easy.framework;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.easy.components.EasyActionType;
import com.easy.model.Assets;
import com.easy.model.ModelServices;
import com.easy.utils.UIBuilder;
import com.easy.model.AppModel;

public abstract class EasyLabelWidget extends EasyWidget {

	private Label label;
	
	public EasyLabelWidget (String text) {
		UIBuilder.setFont(Assets.FONT_MD);
		UIBuilder.setDisplayText("Item not found!");
		UIBuilder.setEasyActionType(EasyActionType.VOID);
		label = UIBuilder.getLabel();
		getTable().add(label).fillX().expandX();
	}

	protected Label getLabel () {
		return this.label;
	}
	
}
