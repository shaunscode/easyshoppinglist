package com.easy.framework;

import com.easy.model.Assets;

public abstract class EasyDialogWidget extends EasyWidget {

	private EasyWidget targetWidget= null;
	
	public EasyDialogWidget () {
		getTable().setBackground(Assets.BG_DIALOG_WIDGET);
	}
	
	public void setTargetWidget (EasyWidget w) {
		this.targetWidget = w;
	}
	
	public EasyWidget getTargetWidget () {
		return this.targetWidget;
	}
	
	public void close () {
		this.remove();
	}
	
}
