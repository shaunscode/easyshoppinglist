package com.easy.framework;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.easy.components.EasyActionHandling;
import com.easy.components.EasyUpdating;
import com.easy.model.ModelServices;

public abstract class EasyController implements EasyActionHandling {

	private static ModelServices modelServices;
	private EasyUpdating view;
	
	public void setModelServices (ModelServices modelServices) {
		EasyController.modelServices = modelServices;
	}

	public void setView (EasyUpdating viewToUpdate) {
		view = viewToUpdate;
	}
	
	public void updateView () {
		view.update(modelServices);
		System.out.println(view);
	}
	
	protected ModelServices getModelServices () {
		return modelServices;
	}

}
