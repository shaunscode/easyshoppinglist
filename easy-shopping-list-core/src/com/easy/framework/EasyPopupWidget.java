package com.easy.framework;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.easy.model.Assets;
import com.easy.model.Constants;
import com.easy.model.ModelServices;
import com.easy.utils.UIBuilder;

public class EasyPopupWidget extends EasyWidget implements Comparable<EasyPopupWidget> {
	
	private String msg;
	private long timePosted;
	private long timeShown;
	private long timeToHide;
	private Label label;
	
	public EasyPopupWidget(String msg) {
		this.msg = msg;
		this.timePosted = System.currentTimeMillis();
		this.addAction(Actions.alpha(0.0f));
		this.pad(200 * Constants.SCALE_WIDTH);
		
		UIBuilder.setFont(Assets.FONT_SM);
		UIBuilder.setFontColor(Color.WHITE);
		UIBuilder.setDisplayText(msg);
		label = UIBuilder.getLabel();
		label.setAlignment(Align.center);
		UIBuilder.setFontColor(Constants.COLOR_LABELS);
		
		
		Container container = new Container();
		container.setActor(label);
		container.background(Assets.SCROLL_BAR.tint(new Color(0.0f, 0.0f, 0.0f, 0.7f))).fillX().pad(50 * Constants.SCALE_WIDTH);
		
		getTable().add(container).expand().fillX().center().align(Align.bottom).padBottom(300 * Constants.SCALE_HEIGHT);
		
		UIBuilder.setFont(Assets.FONT_MD);
		this.fill();
	}

	@Override
	public int compareTo(EasyPopupWidget other) {
		return (this.timePosted < other.timePosted ? -1 : 1);

	}

	public void show () {
		timeShown = System.currentTimeMillis();
		long totalDuration = (long) ((Constants.POPUP_SHOW_TIME + Constants.FADE_DURATION + Constants.FADE_DURATION) * 1000);
		this.timeToHide = (long) (System.currentTimeMillis() + totalDuration);
		System.out.println("Popup shown at: " + timeShown + ", timeToHide: " + timeToHide + ", duraction: " + totalDuration);
		this.addAction(Actions.alpha(1.0f, Constants.FADE_DURATION));
		this.addAction(Actions.delay(Constants.POPUP_SHOW_TIME, Actions.alpha(0.0f, Constants.FADE_DURATION)));
	}
	
	public boolean isFinished () {
		long currentTime = System.currentTimeMillis();
		if (currentTime > this.timeToHide) {
			System.out.println("Popup finished at: " + currentTime + ", timeToHide: " + timeToHide + ", duration was; " + (timeToHide - timeShown));
			return true;
		}
		return false;
	}
	
	@Override
	public void update(ModelServices modelServices) {
		// TODO Auto-generated method stub
		
	}

}
