package com.easy.framework;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.easy.components.EasyActionHandling;
import com.easy.model.Assets;
import com.easy.model.Constants;
/*
 * Idea behind widget is that it used just like a table. 
 * Add Actors, expand/fill etc the cells.
 * Common to all widgets is a padded border and default of the widget border (light grey)
 * Holds display text value, alternative text, and help text.
 * Help text will be displayed/not in combo with the options model value set by the options view/controller
 */
public abstract class EasyWidget extends EasyElement {

	private String displayText = "";
	private String alternateText = "";
	private String helpText = "";

	public EasyWidget() {
		this.fillX();
		this.setActor(getTable());
		this.getTable().pad(Constants.LABEL_PADDING_TOP, Constants.LABEL_PADDING_LEFT, Constants.LABEL_PADDING_BOTTOM, Constants.LABEL_PADDING_RIGHT);
		//this.background(Assets.PANEL);
	}

	public String getDisplayText() {
		return displayText;
	}

	public void setDisplayText(String displayText) {
		this.displayText = displayText;
	}

	public String getAlternateText() {
		return alternateText;
	}

	public void setAlternateText(String alternateText) {
		this.alternateText = alternateText;
	}

	public String getHelpText() {
		return helpText;
	}

	public void setHelpText(String helpText) {
		this.helpText = helpText;
	}



}
