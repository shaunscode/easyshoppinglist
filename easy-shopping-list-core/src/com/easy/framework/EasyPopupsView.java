package com.easy.framework;

import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.easy.model.Assets;
import com.easy.model.ModelServices;

public class EasyPopupsView extends EasyView {
	
	private static Array<EasyPopupWidget> msgs = new Array<EasyPopupWidget>();
	
	private EasyPopupWidget currentShowing = null;
	
	public EasyPopupsView () {
		setFillParent(true);
	}	
	
	public static void post (String msg) {
		msgs.add(new EasyPopupWidget(msg));
	}
	
	@Override
	public void update (ModelServices modelServices) {
		if (currentShowing == null) {
			if (msgs.size > 0) {
				currentShowing = msgs.get(0);
				msgs.removeIndex(0);
				setActor(currentShowing);
				currentShowing.show();
			}			
		} else if (currentShowing.isFinished()) {
			currentShowing.remove();
			currentShowing = null;
		}
		
		
	}
}
