package com.easy.components;

public enum EasyViewType {
	ROOT ("Root view"),

	TITLE ("Title view"),
	MENU ("Menu view"),
	
	OPTIONS ("Options view"), 
	LIST ("List view"), 
	CATALOG ("Catalog view"), 
	CALC ("Calculator view");
	
	private String displayName;
	
	private EasyViewType (String displayName) {
		this.displayName = displayName;
	}
	
	public String toString () {
		return displayName;
	}
	
	
}
