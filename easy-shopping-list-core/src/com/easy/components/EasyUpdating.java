package com.easy.components;

import com.easy.model.ModelServices;

public interface EasyUpdating {
	public void update (ModelServices modelServices);
}
