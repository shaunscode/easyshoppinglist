package com.easy;


import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.easy.controllers.AppController;
import com.easy.controllers.CatalogItemsViewController;
import com.easy.controllers.CatalogViewController;
import com.easy.controllers.DialogController;
import com.easy.controllers.ListViewController;
import com.easy.controllers.OverlayViewController;
import com.easy.framework.EasyStage;
import com.easy.controllers.OptionsViewController;
import com.easy.model.AppModel;
import com.easy.model.Assets;
import com.easy.model.Constants;
import com.easy.model.ModelServices;
import com.easy.utils.UIBuilder;
import com.easy.views.RootView;


public class Run extends ApplicationAdapter {

	public static boolean resumingApplication = true;
	
	private EasyStage stage; //aka view
	private ModelServices modelServices;	//aka model
	private AppController controller;	//well, controller..
	
	@Override
	public void create () {
		
		//must be done first so assets are available for the view's to be built 
		Assets.loadAssets();
				
		//create the MVC components
		modelServices = new ModelServices(new AppModel());
		controller = new AppController();
		
		//set the builder for building the view (must include controller for libgdx ui event listeners to throw actions to)
		UIBuilder.setFont(Assets.FONT_MD);
		UIBuilder.setEasyActionHandler(controller);		
		
		
		//build the view (must be after UIBuilder  has been set)
		RootView view = new RootView();
		stage = new EasyStage(view);
		
		//setup the controller for setting the model and passingit to the view for updating
		controller.setModelServices(modelServices);
		controller.setView(stage);
		
		controller.addController(new OverlayViewController());
		controller.addController(new CatalogItemsViewController());
		controller.addController(new CatalogViewController());
		controller.addController(new ListViewController());
		controller.addController(new OptionsViewController());
		controller.addController(new DialogController());
		
		
		Gdx.input.setInputProcessor(stage.getInputProcessor());

		stage.update(modelServices);
		
		//Gdx.graphics.setContinuousRendering(false);
		//Gdx.graphics.requestRendering();
	}
	
	@Override 
	public void resume () {
		Run.setResumingApplication(true);
		Assets.loadAssets();
	}
	

	@Override
	public void render () {
		super.render();
		
		
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );
		
		stage.refresh();//calls the libgdx stage's act/draw methods 
		//stage.update(modelServices); //needs regular updating to show/remove popups
		
	}
	
	@Override
	public void resize (int width, int height) {
		Constants.VIEW_PORT.update(width, height);
	}
	
	@Override
	public void dispose () {
		modelServices.saveModelData();
		
		return;
	}

	
	
	public static boolean isResumingApplication() {
		return resumingApplication;
	}

	public static void setResumingApplication(boolean resumingApplication) {
		Run.resumingApplication = resumingApplication;
	}

	
	
}