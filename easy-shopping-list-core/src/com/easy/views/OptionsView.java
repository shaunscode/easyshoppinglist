package com.easy.views;

import com.easy.components.EasyActionType;
import com.easy.controllers.OptionsViewController;
import com.easy.framework.EasyElement;
import com.easy.framework.EasyView;
import com.easy.framework.EasyController;
import com.easy.model.Constants;
import com.easy.model.ModelServices;
import com.easy.widgets.OptionToggleWidget;

public class OptionsView extends EasyView {

	public OptionsView () {
		
		super(true); //makes the view able to scroll
		
		addWidget(new OptionToggleWidget(Constants.OPTION_GROUP_CATALOG, "Group selected catalog items", "Items added to the shopping list will be highlighted and moved to the top of the catalog list", EasyActionType.OPTIONS_TOGGLE_GROUP_CATALOG_ITEMS));
		addWidget(new OptionToggleWidget(Constants.OPTION_GROUP_LIST, "Group selected list items", "Items checked off the shopping list will be dimmed and moved to the bottom of the shopping list", EasyActionType.OPTIONS_TOGGLE_GROUP_LIST_ITEMS));
	}
	
	
	
}
