 package com.easy.views;

public enum MenuViews {
	CATALOG("Catalog"), 
	LIST("List"), 
	CALC("Calc"),
	OPTIONS("Options");
	
	private String displayText;
	
	private MenuViews (String toString) {
		displayText = toString;
	}

	public String toString () {
		return displayText;
	}

};
