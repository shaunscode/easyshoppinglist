package com.easy.views;

import com.easy.framework.EasyMultiView;
import com.easy.framework.EasyView;
import com.easy.model.Constants;
import com.easy.model.ModelServices;
import com.easy.widgets.MenuWidget;

public class MainView extends EasyMultiView {

	public MainView () {
		
		
		EasyView catalogView = new CatalogView();
		EasyView listView = new ListView();
		EasyView calcView = new CalcView();
		EasyView optionsView = new OptionsView();
		
		this.addView(catalogView, MenuViews.CATALOG.toString());
		this.addView(listView, MenuViews.LIST.toString());
		this.addView(calcView, MenuViews.CALC.toString());
		this.addView(optionsView, MenuViews.OPTIONS.toString());
		
		this.setView(MenuViews.CATALOG.toString());
		
	}

	
	@Override
	public void update (ModelServices modelServices) {
		super.update(modelServices);
		Object cachedSelectedMenu = modelServices.getCached(Constants.CACHE_SELECTED_MENU);
		MenuViews selectedMenu = (MenuViews)cachedSelectedMenu;
		setView(selectedMenu.toString());
		}
	
}
