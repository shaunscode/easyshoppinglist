package com.easy.views;

import com.easy.framework.EasyPopupsView;
import com.easy.framework.EasyView;
import com.easy.model.Constants;
import com.easy.model.ModelServices;
import com.easy.widgets.CatalogFilterWidget;

public class CatalogView extends EasyView {


	private CatalogFilterWidget filterWidget;
	private CatalogItemsView cataogItemsView;
	
	
	public CatalogView () {
		
		filterWidget = new CatalogFilterWidget();
		cataogItemsView = new CatalogItemsView();
		
		addView(cataogItemsView);
		layer();
		addWidget(filterWidget);

	}
	
	@Override
	public void update(ModelServices modelServices) {
		
		MenuViews selectedView = (MenuViews) modelServices.getCached(Constants.CACHE_SELECTED_MENU);
		
		if (selectedView.equals(MenuViews.CATALOG)) {
			
			long start = System.currentTimeMillis();
			cataogItemsView.update(modelServices);
			filterWidget.update(modelServices);
			
			long timeTaken = System.currentTimeMillis() - start;
			float msecs = timeTaken;
			//EasyPopupsView.post(msecs +  "msecs to update CatalogView");
			
			
		}
	}

}
