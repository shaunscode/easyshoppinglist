package com.easy.views;

import com.easy.framework.EasyLabelWidget;
import com.easy.framework.EasyView;
import com.easy.model.AppModel;
import com.easy.model.Constants;
import com.easy.model.ModelServices;

public class CalcView extends EasyView {

	public CalcView () {
		
	}
	

	@Override
	public void update(ModelServices modelServices) {
		//dont bother updating if this isn't being viewed
		MenuViews selectedView = (MenuViews) modelServices.getCached(Constants.CACHE_SELECTED_MENU);
		if (!selectedView.equals(MenuViews.CALC)) {
			return;
		}

	}

}
