package com.easy.views;

import com.badlogic.gdx.utils.Array;
import com.easy.framework.EasyView;
import com.easy.model.Constants;
import com.easy.model.ModelServices;
import com.easy.model.ShopItem;
import com.easy.utils.CatalogItemWidgetComparator;
import com.easy.widgets.CatalogFilterOptionsWidget;
import com.easy.widgets.CatalogFilterWidget;
import com.easy.widgets.CatalogItemWidget;
import com.easy.widgets.SaveFilterItemWidget;

public class CatalogItemsView extends EasyView {

	private CatalogItemWidgetComparator catalogItemWidgetComparator;
	
	private CatalogFilterOptionsWidget itemNotFoundWidget;
	private SaveFilterItemWidget saveFilterItemWidget;
	

	
	public CatalogItemsView () {
		getTable().padBottom(Constants.ICON_SIZE_LG * 2);
		itemNotFoundWidget = new CatalogFilterOptionsWidget();
		saveFilterItemWidget = new SaveFilterItemWidget ();
		catalogItemWidgetComparator = new CatalogItemWidgetComparator();
		scrollable();
		
		
	}


	
	@Override
	public void update(ModelServices modelServices) {
		
		//remove all widgets from view
		Array<CatalogItemWidget> displayedWidgets = new Array<CatalogItemWidget>();
		
		//remove all widgets from the displayed wisdgets
		getTable().clearChildren();
		
		//get a list of items in the shop model 
//modelServices.getShopItems().values();
		
		//for each catalog item
		for (String key: modelServices.getShopItems().keySet()) {
			
			ShopItem item = modelServices.getShopItems().get(key);
			
			//if the item passes the filter, add it to the displayed widgets
			if (item.getDisplayText().toUpperCase().contains(modelServices.getCatalogFilterText().trim().toUpperCase())) {
				displayedWidgets.add(new CatalogItemWidget(item.getDisplayText(), item.getId()));
			}
		}
		
		for (CatalogItemWidget catalogItemWidget: displayedWidgets) {
			catalogItemWidget.update(modelServices);
		}
		
		
		
		//sort the displayed widgets
		catalogItemWidgetComparator.setGroupCatalogItems(modelServices.isOptionWidgetChecked(Constants.OPTION_GROUP_CATALOG));
		displayedWidgets.sort(catalogItemWidgetComparator);
		
		
		//add them to the view
		for (CatalogItemWidget catalogItemWidget: displayedWidgets) {
			addWidget(catalogItemWidget);
		}
			
		
		//TODO: this could be tidied up, reversing conditions - the save widget is always displayed unless it matches one of the filter results
		
		// so have to be seaching first for the save widget to display..
		if (modelServices.getCatalogFilterText().length() > 0) { 
			//if only a single result
			if (displayedWidgets.size == 1) {
				//and its not the same as whats type in - like 'taco' is typed in and 'taco shell' is all thats displayed
				if (!displayedWidgets.get(0).getDisplayText().toUpperCase().equals(modelServices.getCatalogFilterText().trim().toUpperCase())) {
					//so 'taco' is typed in but only 'taco shells' i displayed
					//add the widget, padding the bottom to clear the menu buttons
					addWidget(saveFilterItemWidget).padBottom(15);	
				}
			//so there are more than one result
			} else if (displayedWidgets.size > 1) {
				//go through all visible results, if none match the exact filter text, display save widget
				boolean display = true;
				for (CatalogItemWidget catalogItemWidget: displayedWidgets) {
					if (catalogItemWidget.getDisplayText().toUpperCase().equals(modelServices.getCatalogFilterText().trim().toUpperCase())) {
						display = false;
						break;
					}
				}
				if (display) {
					addWidget(saveFilterItemWidget).padBottom(15);
				}
			} else if (displayedWidgets.size <= 0) {
				addWidget(itemNotFoundWidget);
				addWidget(saveFilterItemWidget).padBottom(15);
			}
			
		}	
		
		//super.update(modelServices);
		
		System.out.println("cat items view:" + this.toString());
	}
	

}
