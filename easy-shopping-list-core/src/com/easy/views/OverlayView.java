package com.easy.views;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Align;
import com.easy.framework.EasyView;
import com.easy.model.Constants;
import com.easy.model.ModelServices;
import com.easy.widgets.CatalogFilterWidget;
import com.easy.widgets.MenuWidget;

public class OverlayView extends EasyView {
	
	private MenuWidget menuWidget;
	private CatalogFilterWidget filterWidget;
	
	public OverlayView () {
		
		setFillParent(true);
	
		filterWidget = new CatalogFilterWidget();
		addWidget(filterWidget).expandY().align(Align.top);
		
		menuWidget = new MenuWidget();
		addWidget(menuWidget).expandY().align(Align.bottom);
	
		//this.padTop(100);
	}

	@Override
	public void update(ModelServices modelServices) {
		// TODO Auto-generated method stub
		super.update(modelServices);
		
		if (!MenuViews.CATALOG.equals(modelServices.getCached(Constants.CACHE_SELECTED_MENU))) {
			filterWidget.addAction(Actions.hide());
		} else {
			filterWidget.addAction(Actions.show());
		}
	}
	
	
	
}
