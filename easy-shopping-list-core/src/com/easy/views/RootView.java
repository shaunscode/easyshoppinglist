package com.easy.views;

import com.easy.framework.EasyLayer;
import com.easy.framework.EasyView;
import com.easy.widgets.MenuWidget;
import com.easy.widgets.TitleWidget;

public class RootView extends EasyView {

	public RootView () {
		
		//add the containers to the base view table
		addWidget(new TitleWidget("My app")).top();
		addView(new MainView()).expand().fill().top();
		
		EasyLayer layer = layer();
		addWidget(new MenuWidget()).expand().bottom();
		
		System.out.println(this);
		this.fill();
	}
	
	

	
	
	
}
