package com.easy.views;

import com.badlogic.gdx.utils.Array;
import com.easy.framework.EasyView;
import com.easy.model.Constants;
import com.easy.model.ModelServices;
import com.easy.model.ShopItem;
import com.easy.utils.ListItemWidgetComparator;
import com.easy.widgets.ListItemWidget;

public class ListView extends EasyView {
	private ListItemWidgetComparator listItemWidgetComparator;
	
	public ListView () {
		super(true); //scrolls	
		listItemWidgetComparator = new ListItemWidgetComparator();
	}
	
	@Override
	public void update(ModelServices modelServices) {
		
		
		Array<ListItemWidget> displayedWidgets = new Array<ListItemWidget>();

		getTable().clearChildren();
		//for each catalog item
		for (String key: modelServices.getShopItems().keySet()) {
					
			ShopItem item = modelServices.getShopItems().get(key);
			
			//if the item passes the filter, add it to the displayed widgets
			if (item.isCheckedCatalog()) {
				displayedWidgets.add(new ListItemWidget(item.getDisplayText(), item.getId()));
			}
		}
		

		//special case updating here
		//widgets need updating before sorting
		//super.update will only update childwidgets
		//but adding them pre-sort adds them out of order
		//bit of circular dependancy, updating here removesthat
		for (ListItemWidget listItemWidget: displayedWidgets) {
			listItemWidget.update(modelServices);
		}
		
		
		
		//sort the displayed widgets
		listItemWidgetComparator.setGroupListItems(modelServices.isOptionWidgetChecked(Constants.OPTION_GROUP_LIST));
		displayedWidgets.sort(listItemWidgetComparator);
		
		//add them to the view
		
		for (ListItemWidget listItemWidget: displayedWidgets) {
			addWidget(listItemWidget);
		}
		
		super.update(modelServices);
		
		
		
				/*
		this.removeAllElements();
		
		//get the shop items as they are in the model
		HashMap<String, ShopItem> shopItems = modelServices.getShopItems();		
		
		//for each item in the model
		for (String key: shopItems.keySet()) {
			
			ShopItem shopItem = shopItems.get(key);
			ListItemWidget itemWidget = null;

			//is it highlighted in the catalog? if so, it should be on this list
			if (shopItem.isCheckedCatalog()) {	
				
				//if it is not in current list of widgets,
				if (!listItemWidgets.containsKey(shopItem.getId())) {
					//create and add it
					itemWidget = new ListItemWidget(shopItem.getDisplayText(), shopItem.getId());
					listItemWidgets.put(itemWidget.getId(), itemWidget);
				} else {
					itemWidget = listItemWidgets.get(shopItem.getId());
				}
				itemWidget.setChecked(shopItem.isCheckedList());
				itemWidget.update(modelServices);	//let the widget update itself
			} else {
				//isn't highlighted i the catalog, so remove if it is in the current list of widgets
				listItemWidgets.remove(shopItem.getId());
			}
			
		}
		
		
		//move all the items to an array to then sort
		Array<ListItemWidget> listItemsArray = new Array<ListItemWidget>();
		for (String key: listItemWidgets.keySet()) {
			listItemsArray.add(listItemWidgets.get(key));
		}
		
		listItemWidgetComparator.setGroupListItems(modelServices.isOptionWidgetChecked(Constants.OPTION_GROUP_LIST));
		listItemsArray.sort(listItemWidgetComparator);
		
		for (ListItemWidget listItemWidget: listItemsArray) {
			this.addWidget(listItemWidget);
		}
		*/
	}
	

}
