package com.easy.model;

import java.util.HashMap;
import java.util.Set;

import com.badlogic.gdx.Preferences;
import com.easy.EasyDebug;
import com.easy.framework.EasyModel;

public class ShopModel extends EasyModel {

		private HashMap<String, ShopItem> shopItems;
		
		
		public ShopModel (Preferences saveData) {
			super(saveData, Constants.SHOP_DATA);	
		}
		
		protected void init () {
			shopItems = new HashMap<String, ShopItem>();
		}
		
		protected String getDefaultModelDataString () {
			//new string to put 
			StringBuilder shopModelDataString = new StringBuilder();
			
			//get the items
			String[] defaultItemsArray = getDefaultShopItems();
			
			//add each item to the shopModelData
			for (int i = 0; i < defaultItemsArray.length; i++) {
				if (i != 0) {
					shopModelDataString.append(",");
				}
				shopModelDataString.append(defaultItemsArray[i]);
				shopModelDataString.append(",0");
				shopModelDataString.append(",false");	//checked on catalog
				shopModelDataString.append(",false");	//checked on list
				shopModelDataString.append(",false");	//favourite
			}
		
	
			return shopModelDataString.toString();
	}
	
	private String[] getDefaultShopItems () {
		//long string of items comma deliminated
		String defaultItems = "Apples,Apples,Bananas,Berries,Grapes,Lemons,Lime,Melons,Nectarines,Oranges,Peaches,Pears,Plums," +
					"Strawberries,Watermelon,Asparagus,Broccoli,Cabbage,Carrots,Cauliflower,Celery,Corn,Garlic,Lettuce," +
				"Mushrooms,Onions,Peppers,Potato,Squash,SweetPotato,Tomatoes,Zucchini,Cherries,Mixed Fruit," +
				"Peaches,Pears,Pineapples,Asparagus,Carrots,Corn,Greenbeans,Peas,Potatoes,Tomatoes," +
				"Baked Beans,Butter Beans,Green Beans,Kidney Beans,Pinto Beans,Pork N Beans,String Beans,Broccoli," +
				"Carrots,Corn,Dinners,French Fries,Ice Cream,Mixed Veg.,Peas,Pizza,Tater Tots,Ground Beef," +
				"Hamburger Beef,Roast Beef,Steaks Beef,Chicken Breast,Chicken Breast with Bone,Chicken Legs,Chicken Whole," +
				"Chicken Wings,Ham,Roast Beef,Smoked Turkey,Turkey,Bacon,Chops,Ham,Roast,Sausage,Crabmeat," +
				"Fish,Scallops,Shrimp,Canned Chicken,Canned Corned Beef,Canned Ham,Canned Salmon,Canned Tuna," +
				"Canned Vienna Sausage,Celery Salt,Cinnamon,Garlic Powder,Garlic Salt,Ginger,Nutmeg,Onion Powder," +
				"Oregano,Paprika,Parsley,Pepper,Salt,BBQ Sauce,Honey,Horseradish,Jelly,Ketchup,Mayonnaise," +
				"Mustard,Peanut Butter,Salsa,Soy Sauce,Syrup,Worcestershire,Batteries,Cards,Camera Film," +
				"LightBulbs,VCR Tapes,Cooking Spray,Olive Oil,Vegetable Oil,Blue Cheese,French,Italian,Ranch," +
				"Thousand Island,Candy,Cookies,Crackers,Nuts,Popcorn,PotatoChips,Pretzels,Raisins,Chickenand Rice," +
				"Chicken Noodle,Cream of Broccoli,Creamof Celery,Creamof Chicken,Creamof Mushroom,Tomato," +
				"Vegetable,Vegetable Beef,Vegetable Chicken,Biscuits,Buns,Hamburger Buns,Hot Dog Buns,French Buns," +
				"Italian Buns,Rolls,Wheat Buns,White Buns,Coffee,Juice,Milk,Orange Juice,Soft Drinks,Sports Drinks," +
				"Tea,Water,Butter,Cheese,Cottage Cheese,Cream Cheese,Creamer,Eggs,Margarine,Milk," +
				"Sliced Cheese,Sour Cream,Yogurt,Spaghetti Sauce,Tomato Paste,Tomato Sauce,Angel Hair Pasta," +
				"Elbow Macaroni,Lasagne Pasta,Rotelle Pasta,Shells Pasta,Spaghetti Pasta,Vemicelli Pasta,Green Chili," +
				"Refried Beans,Salsa,Spanish Rice,Tacos,Tortillias-Corn,Tortillias-Flour,Baking Powder,Baking Soda," +
				"Brown Sugar,Brownie Mix,Cake Mix,Cereal,Cocoa,Cornstarch,Flour,Jello,Oatmeal,Pancake Mix," +
				"Rice,Sugar,Vanilla Extract,Diapers,Formula,Lotion,Oil,Rash Cream,Shampoo,Wipes,Apple Juice," +
				"Applesauce,Bananas,Beef,Carrots,Chicken,Dinners,Greenbeans,Mixed Veg,Peaches,Pears,Squash," +
				"Sweet Potato,Veal,Antiacid,Bandaids,Cough Drops,First Aid Cream,Hydrogen Peroxide,Pain-Reliever," +
				"Rubbing Alcohol,Dog Food,Cat Food,Cat Litter,Anti-Bacterial Soap,Deodorant,Floss,Lotion,Mouthwash," +
				"Petroleum Jelly,Razors,Shampoo,Shaving Cream,Soap,Toothbrush,Foil,Freezer Bags,Kitchen Bags," +
				"Paper Towels,Plastic Wrap,Sandwich Bags,Storage Bags,Tissue,Toilet Paper,Trash Bag,Yard Bags," +
				"Air Freshner,Bleach,Broom,Dishwasher Detergent,Dishwashing Soap,Dryer Sheets,Furniture Polish," +
				"Glass Cleaner,Laundry Detergent,Mop,Scrub Brush,Sponges,Toilet Bowl Cleaners,Towels";
	
		//seperate into an array
		String[] defaultItemsArray = defaultItems.split(",");
		
		//return trimmed clean array of items
		return defaultItemsArray;
	}

	

	public ShopItem addShopItem (String displayText, int quantity, boolean checkedCatalog, boolean checkedList, boolean favourite) {
		ShopItem shoppingItem = new ShopItem(displayText, quantity, checkedCatalog, checkedList, favourite);
		if (!shopItems.containsKey(shoppingItem.getId())) {
			shopItems.put(shoppingItem.getId(), shoppingItem);
			return shoppingItem;
		} 
		//otherwise get the one from the map and let newly created oneby garbage collected
		return shopItems.get(shoppingItem.getId());
	}
	
	
	
	
	
	
	
	
	
	public HashMap<String, ShopItem> getShopItems () {
		return shopItems;
	}

	@Override
	protected String generateSaveDataString() {
		StringBuilder shopItemsString = new StringBuilder();
		
		Set<String> keys = getShopItems().keySet();
		for (String key: keys) {
			ShopItem shopItem = getShopItems().get(key);
			shopItemsString.append("" + shopItem.getDisplayText() + "," + shopItem.getQuantity() + "," + shopItem.isCheckedCatalog()+ "," + shopItem.isCheckedList() + "," + shopItem.isFavourite() + ",");
		}
		String shopModelDataString = shopItemsString.substring(0, shopItemsString.length() - 1);	//trim that last comma off
		return shopModelDataString;
	}


	
	
	
	@Override
	protected void populateModel ( ) {
		//split the data up into an array 
		String[] shopModelDataArray = saveData.getString(Constants.SHOP_DATA).split(",");

		for (int i = 0; i < shopModelDataArray.length; i += 5) {
			
			String displayText = shopModelDataArray[i];
			int quantity = Integer.parseInt(shopModelDataArray[i + 1]);
			boolean checkedCatalog = (shopModelDataArray[i + 2].equalsIgnoreCase("false")? false: true);
			boolean checkedList = (shopModelDataArray[i + 3].equalsIgnoreCase("false")? false: true);
			boolean favourite = (shopModelDataArray[i + 4].equalsIgnoreCase("false")? false: true);
			
			addShopItem(displayText, quantity, checkedCatalog, checkedList, favourite);
		}
		
	}
	
	
	
	
}
