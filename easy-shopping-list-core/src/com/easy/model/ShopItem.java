package com.easy.model;


public class ShopItem implements Comparable<ShopItem> {

	private String id;
	private String displayText;
	private int quantity;
	private boolean checkedCatalog;
	private boolean checkedList;
	private boolean showingDeleteContainer;
	private boolean showingOptionsContainer;
	private boolean favourite;

	public ShopItem (String displayText, int quantity, boolean checkedCatalog, boolean checkedList, boolean favourite) {
		
		
		this.displayText = (displayText.substring(0, 1).toUpperCase() + displayText.substring(1)).trim();
		this.id = displayText.replace(" ",  "");
		this.quantity = quantity;
		this.checkedCatalog = checkedCatalog;
		this.checkedList = checkedList;
		this.favourite = favourite;
		this.showingDeleteContainer = false;	//is a cacheed value only, not stored to disk and defaults to false (not showing)
		this.showingOptionsContainer = false;
	}
	
	public String getId () {
		return this.id;
	}
	
	public String getDisplayText () {
		return this.displayText;
	}	

	@Override
	public boolean equals (Object otherItem) {
		return this.getId().equals(((ShopItem)otherItem).getId());
		
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public boolean isCheckedCatalog() {
		return checkedCatalog;
	}

	
	public boolean isCheckedList() {
		return checkedList;
	}


	public void setCheckedCatalog(boolean checked) {
		this.checkedCatalog = checked;
	}

	public void setCheckedList(boolean checked) {
		this.checkedList = checked;
	}

	public boolean isShowingDeleteContainer() {
		return showingDeleteContainer;
	}

	public void setShowingDeleteContainer(boolean confirmingDelete) {
		this.showingDeleteContainer = confirmingDelete;
	}

	public boolean isShowingOptionsContainer() {
		return showingOptionsContainer;
	}

	public void setShowingOptionsContainer(boolean showingOptionsContainer) {
		this.showingOptionsContainer = showingOptionsContainer;
	}

	public boolean isFavourite() {
		return favourite;
	}

	public void setFavourite(boolean favourite) {
		this.favourite = favourite;
	}
	
	
	
	public String toString () {
		StringBuilder sb = new StringBuilder();
		
		sb.append("id:" + id);
		sb.append(", DisplayText:" + displayText);
		sb.append("\n\tqty:" + quantity) ;
		sb.append(", checkedCatalog:" + checkedCatalog);
		sb.append(", checkedList:" + checkedList);
		sb.append(", showingDelete:" + showingDeleteContainer);
		sb.append(", showingOptions:" + showingOptionsContainer);
		sb.append(", fav:" + favourite);
		
		return sb.toString();
		
	}

	@Override
	public int compareTo(ShopItem other) {
		return this.getDisplayText().toUpperCase().compareTo(other.getDisplayText().toUpperCase());
	}
	
}
