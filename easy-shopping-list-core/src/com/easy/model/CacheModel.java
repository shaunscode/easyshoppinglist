package com.easy.model;

import java.util.HashMap;

import com.badlogic.gdx.Preferences;
import com.easy.framework.EasyDialogWidget;
import com.easy.framework.EasyModel;
import com.easy.views.MenuViews;

public class CacheModel extends EasyModel {
	
	private HashMap<String, Object> cacheMap = new HashMap<String, Object>();
	
	
	public CacheModel(Preferences saveData) {
		super(saveData, "cache");
		cacheMap.put(Constants.CACHE_SELECTED_MENU, MenuViews.CATALOG);
		cacheMap.put(Constants.CACHE_DIALOG, null);
		cacheMap.put(Constants.CACHE_CATALOG_FILTER, new Boolean(true));
	}

	@Override
	protected void init() {
		
	}

	@Override
	protected String getDefaultModelDataString() {
		return "";
	}

	@Override
	protected String generateSaveDataString() {
		return ""; //this is only for running data that doesn't need to be persistent
	}

	@Override
	protected void populateModel() {
	}

	public void setObject (String name, Object obj) {
		cacheMap.put(name, obj);
	}
	
	public Object getObject (String name) {
		return cacheMap.get(name);
	}
	
}
