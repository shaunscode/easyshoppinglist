package com.easy.model;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;

public class Assets {
	
	Map<Integer, BitmapFont> sizedFonts = new HashMap<Integer, BitmapFont>(); 
	
	public static Drawable 
		PANEL, 
		BG_DIALOG_WIDGET,
		TOP_BORDER_ONLY,
		FILTER_BOX,
		TITLE_PANEL;
	
	
	public static SpriteDrawable 
		

		BG_SOLID_WHITE,//spritedrawable so it can be tinted 
	
		ICON_XCROSS,
		ICON_PLUS,
		ICON_MINUS,
		ICON_Y,
		ICON_N,
		ICON_SAVE,
		ICON_TICK,
		ICON_STAR,
		ICON_VERTICAL_DOTS,
		ICON_LIST,
		ICON_CATALOG,
		ICON_CALC,
		ICON_OPTIONS,		
		ICON_LIST_FILLED,
		ICON_CATALOG_FILLED,
		ICON_CALC_FILLED,
		ICON_OPTIONS_FILLED,		
		ICON_LIST_CHECKED,
		ICON_CATALOG_CHECKED,
		ICON_CALC_CHECKED,
		ICON_OPTIONS_CHECKED,		
		ICON_TRASHCAN, 
		
		ICON_CHECKBOX,
		ICON_UP_ARROW,
		ICON_DOWN_ARROW,
		ICON_LEFT_ARROW,
		ICON_RIGHT_ARROW,
		
		SCROLL_BAR,
		SCROLL_BACKGROUND;
		
	public static Skin UI_SKIN;
	public static BitmapFont FONT_SM, FONT_MD, FONT_LG;
	
	public static void loadAssets () {
		PANEL = new NinePatchDrawable(new NinePatch(new TextureRegion(new Texture(Gdx.files.internal("bgPanel.png"))), 1, 1, 1 ,1));	
		TOP_BORDER_ONLY = new NinePatchDrawable(new NinePatch(new TextureRegion(new Texture(Gdx.files.internal("bgTopBorderOnly.png"))), 1, 1, 1 ,1));
		BG_DIALOG_WIDGET = new NinePatchDrawable(new NinePatch(new TextureRegion(new Texture(Gdx.files.internal("bgDialogWidget.png"))), 1, 1, 1 ,1));
		
		FILTER_BOX = new NinePatchDrawable(new NinePatch(new TextureRegion(new Texture(Gdx.files.internal("bgPanel.png"))), 1, 1, 1, 1));	
		TITLE_PANEL = new NinePatchDrawable(new NinePatch(new TextureRegion(new Texture(Gdx.files.internal("bgTitlePanel.png"))), 2, 2, 2, 2));	
		
		BG_SOLID_WHITE = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("bgSolidWhite.png"))));
		
		ICON_XCROSS = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("icon_xcross.png"))));
		ICON_Y = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("icon_y.png"))));
		ICON_N = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("icon_n.png"))));
		ICON_SAVE = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("icon_save.png"))));
		ICON_TICK = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("icon_tick.png"))));
		ICON_PLUS = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("icon_plus.png"))));
		ICON_MINUS = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("icon_minus.png"))));		
		ICON_STAR = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("icon_star.png"))));		
		ICON_VERTICAL_DOTS = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("icon_vertical_dots.png"))));		
		ICON_LIST = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("icon_list.png"))));		
		ICON_CATALOG = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("icon_catalog.png"))));		
		ICON_CALC = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("icon_calc.png"))));		
		ICON_OPTIONS = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("icon_options.png"))));			
		ICON_LIST_FILLED = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("icon_list_filled.png"))));		
		ICON_CATALOG_FILLED = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("icon_catalog_filled.png"))));		
		ICON_CALC_FILLED = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("icon_calc_filled.png"))));		
		ICON_OPTIONS_FILLED = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("icon_options_filled.png"))));			
		ICON_LIST_CHECKED = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("icon_list_checked.png"))));		
		ICON_CATALOG_CHECKED = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("icon_catalog_checked.png"))));		
		ICON_CALC_CHECKED = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("icon_calc_checked.png"))));		
		ICON_OPTIONS_CHECKED = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("icon_options_checked.png"))));			
		ICON_UP_ARROW = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("icon_up_arrow.png"))));		
		ICON_DOWN_ARROW = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("icon_down_arrow.png"))));		
		ICON_LEFT_ARROW = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("icon_left_arrow.png"))));		
		ICON_RIGHT_ARROW = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("icon_right_arrow.png"))));		
		ICON_TRASHCAN = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("icon_trashcan.png"))));		
		SCROLL_BAR = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("scrollbar_bar.png"))));
		SCROLL_BACKGROUND = new SpriteDrawable(new Sprite(new Texture(Gdx.files.internal("scrollbar_background.png"))));
		
		
		UI_SKIN = new Skin(Gdx.files.internal("appSkin.json"));
		FONT_SM = getBitmapFontSize(Constants.FONT_SIZE_SM);
		FONT_MD = getBitmapFontSize(Constants.FONT_SIZE_MD);
		FONT_LG = getBitmapFontSize(Constants.FONT_SIZE_LG);
	}
	
	private static int getGraphicsDensity () {
		float originalDensity = Gdx.graphics.getDensity();
		float graphicsDensity = originalDensity > 1? originalDensity: 1;		
		return (int)(graphicsDensity);
	}

	private static BitmapFont getBitmapFontSize (int fontSize) {
		long timerStart = System.nanoTime();
		FreeTypeFontGenerator fontGen = new FreeTypeFontGenerator(Gdx.files.internal("Roboto-Light.ttf"));
		FreeTypeFontParameter fontParam = new FreeTypeFontParameter();
		fontParam.size = getGraphicsDensity() * fontSize;
		long timerEnd = System.nanoTime() - timerStart;
		float time = ((float)timerEnd) / 1000000000;
		//System.out.println("Took " + time + "msecs to make font size " + fontSize);
		return fontGen.generateFont(fontParam);
		
	}
		
}
	
