package com.easy.model;

import java.util.HashMap;

import com.badlogic.gdx.Preferences;
import com.easy.framework.EasyModel;

public class OptionsModel extends EasyModel {

	//model vars to store the data
	private String catalogFilterText = "";
	
	private HashMap<String, String> optionsMap;
	
	public OptionsModel (Preferences saveData ) {
		super(saveData, Constants.OPTIONS_DATA);

	}

	@Override
	protected void init() {
		optionsMap = new HashMap<String, String>();
		catalogFilterText = "";	//default on load of app to be empty
	}

	public void setCatalogFilterText (String text) {
		this.catalogFilterText = text;
	}
	
	public String getCatalogFilterText () {
		return catalogFilterText;
	}
	

	@Override
	protected String getDefaultModelDataString() {
		return "" + Constants.OPTION_GROUP_CATALOG + ", false, " + Constants.OPTION_GROUP_LIST + ", false";
	}

	protected boolean isOptionChecked (String id) {
		return Boolean.parseBoolean(optionsMap.get(id));
	}
	
	public void setOptionChecked (String id, boolean groupSelectedCatalogItems) {
		optionsMap.put(id, Boolean.toString(groupSelectedCatalogItems));
	}

	@Override
	protected String generateSaveDataString() {
		StringBuilder saveString = new StringBuilder();
		
		for (String key: optionsMap.keySet()) {
			saveString.append(key + ",");
			saveString.append(optionsMap.get(key) + ",");
		}
		return saveString.toString();
	}

	@Override
	protected void populateModel() {
		String[] optionsModelDataArray = saveData.getString(Constants.OPTIONS_DATA).split(",");
		
		for (int i = 0; i < optionsModelDataArray.length; i += 2) {
			optionsMap.put(optionsModelDataArray[i], optionsModelDataArray[i + 1]);
		}
	}
}
