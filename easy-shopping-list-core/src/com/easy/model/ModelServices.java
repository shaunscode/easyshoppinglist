package com.easy.model;

import java.util.HashMap;

import com.badlogic.gdx.utils.Array;

public class ModelServices {

	private AppModel model;
	
	public ModelServices (AppModel model) {
		this.model = model;
	}
	
	public CacheModel getCache () {
		return model.getCacheModel();
	}
	
	
	public void closeAllItemOptionButtons () {
		for (String itemKey: model.getShopModel().getShopItems().keySet()) {
			ShopItem item = model.getShopModel().getShopItems().get(itemKey);
			String itemId = item.getId();
			if (isShopItemShowingOptionsContainer(itemId)) {
				setShopItemShowingOptionsContainer(itemId, false);
			}
		}
	}
	
	public void incrementShopItemQuantity (String itemId) {
		ShopItem item = getShopItemById(itemId);
		if (item == null) {
			System.out.println("Error: incrementShopItemQuantity(): shop item not found");
			return;
		}
		int newQty = item.getQuantity() + 1;
		item.setQuantity(newQty);
		
	}

	public void decrementShopItemQuantity (String itemId) {
		ShopItem item = getShopItemById(itemId);
		if (item == null) {
			System.out.println("Error: incrementShopItemQuantity(): shop item not found");
			return;
		}
		int newQty = item.getQuantity() - 1;
		newQty = (newQty < 0? 0: newQty);	//ensure don't have negative qty's
		item.setQuantity(newQty);
		
	}

	
	public void addShopItem (String displayText, int quantity, boolean checkedCatalog, boolean checkedList, boolean favourite) {
		model.getShopModel().addShopItem(displayText, quantity, checkedCatalog, checkedList, favourite);
	}
	
	public void removeShopItem (String itemId) {
		ShopItem item = model.getShopModel().getShopItems().remove(itemId);
		if (item == null) {
			System.out.println("### removeShopItem: couldn't locate item by id '" + itemId + "'..");
		}
	}
	
	public HashMap<String, ShopItem> getShopItems () {
		return model.getShopModel().getShopItems();
	}
		
	public void setShopItemQuantity (String itemId, int quantity) {
		ShopItem item = getShopItemById(itemId);
		if (item == null) {
			System.out.println("ShopModel.setItemQuantity() :: couldn't find shop item with itemId '" + itemId + "'.");
			return;
		}
		item.setQuantity(quantity);
	}
	
	public int getShopItemQuantity (String itemId) {
		ShopItem item = getShopItemById(itemId);
		if (item == null) {
			System.out.println("ShopModel.getItemQuantity() :: couldn't find shop item with itemId '" + itemId + "'.");
			return -1;
		}
		return item.getQuantity();
	}
	
	public void setShopItemCheckedCatalog (String itemId, boolean checked) {
		ShopItem item = getShopItemById(itemId);
		if (item == null) {
			System.out.println("ShopModel.setCheckedCatalog() :: couldn't find shop item with itemId '" + itemId + "'.");
			return;
		}
		item.setCheckedCatalog(checked);		
	}
	
	public void setShopItemCheckedList (String itemId, boolean checked) {
		ShopItem item = getShopItemById(itemId);
		if (item == null) {
			System.out.println("ShopModel.setCheckedList() :: couldn't find shop item with itemId '" + itemId + "'.");			
			return;
		}
		item.setCheckedList(checked);		
	}
	
	public boolean isShopItemCheckedCatalog (String itemId) {
		ShopItem item = getShopItemById(itemId);
		if (item == null) {
			System.out.println("ShopModel.isChecked() :: couldn't find shop item with itemId '" + itemId + "'.");
			return false;
		}
		return item.isCheckedCatalog();
	}


	public boolean isShopItemFavourite (String itemId) {
		ShopItem item = getShopItemById(itemId);
		if (item == null) {
			System.out.println("ShopModel.isShopItemFavourite() :: couldn't find shop item with itemId '" + itemId + "'.");
			return false;
		}
		return item.isFavourite();
	}

	
	public void setShopItemFavourite (String itemId, boolean fav) {
		ShopItem item = getShopItemById(itemId);
		if (item == null) {
			System.out.println("ShopModel.setShopItemFavourite() :: couldn't find shop item with itemId '" + itemId + "'.");			
			return;
		}
		item.setFavourite(fav);		
	}
	
	
	public boolean isShopItemCheckedList (String itemId) {
		ShopItem item = getShopItemById(itemId);
		if (item == null) {
			System.out.println("ShopModel.isChecked() :: couldn't find shop item with itemId '" + itemId + "'.");
			return false;
		}
		return item.isCheckedList();
	}

	public void setShopItemShowingDeleteContainer (String itemId, boolean confirm) {
		ShopItem item = getShopItemById(itemId);
		if (item == null) {
			System.out.println("ShopModel.setShopItemConfirmingDelete() :: couldn't find shop item with itemId '" + itemId + "'.");			
			return;
		}
		item.setShowingDeleteContainer(confirm);		
	}
	
	public boolean isShopItemShowingDeleteContainer (String itemId) {
		ShopItem item = getShopItemById(itemId);
		if (item == null) {
			System.out.println("ShopModel.isShopItemConfirmingDelete() :: couldn't find shop item with itemId '" + itemId + "'.");			
			return false;
		}
		return item.isShowingDeleteContainer();
	}
	
	
	
	public void setShopItemShowingOptionsContainer (String itemId, boolean confirm) {
		ShopItem item = getShopItemById(itemId);
		if (item == null) {
			System.out.println("ShopModel.setShopItemShowingOptionsContainer() :: couldn't find shop item with itemId '" + itemId + "'.");			
			return;
		}
		item.setShowingOptionsContainer(confirm);		
	}
	
	public boolean isShopItemShowingOptionsContainer (String itemId) {
		ShopItem item = getShopItemById(itemId);
		if (item == null) {
			System.out.println("ShopModel.isShopItemShowingOptionsContainer() :: couldn't find shop item with itemId '" + itemId + "'.");			
			return false;
		}
		return item.isShowingOptionsContainer();
	}
	
	
	public String getShopItemDisplayText (String itemId) {
		ShopItem item = getShopItemById(itemId);
		if (item == null) {
			return "Error: Shopitem not found";
		}
		return item.getDisplayText();
	}
	
	
	
	
	private ShopItem getShopItemById (String itemId) {
		return model.getShopModel().getShopItems().get(itemId);
	}

	
	
	///////////////////////
	//OPTIONS SERVICES
	///////////////////////
	public boolean isOptionWidgetChecked (String optionWidgetId) {
		return model.getOptionsModel().isOptionChecked(optionWidgetId);
	}
	
	public void setOptionWidgetChecked (String optionWidgetId, boolean checked) {
		model.getOptionsModel().setOptionChecked(optionWidgetId, checked);
	}
	
	public void setCatalogFilterText (String text) {
		model.getOptionsModel().setCatalogFilterText(text);
	}
	
	public String getCatalogFilterText () {
		return model.getOptionsModel().getCatalogFilterText();
	}
	
	public void saveModelData () {
		model.saveModelData();
	}


	public Object getCached (String name) {
		return model.getCacheModel().getObject(name);
	}
	
	public void setCached (String name, Object obj) {
		model.getCacheModel().setObject(name, obj);
	}
	
	public void printShopModel () {
	
		Array<ShopItem> items = new Array<ShopItem>();
		for (String key: model.getShopModel().getShopItems().keySet()) {
			items.add(model.getShopModel().getShopItems().get(key));
		}
		
		items.sort();
		
		int i  = 0;
		System.out.println("ShopModel dump of items\n===========================");
		for (ShopItem item: items) {
			if (i++ > 4) 
				break;
			System.out.println(item);
		}
	}
}
