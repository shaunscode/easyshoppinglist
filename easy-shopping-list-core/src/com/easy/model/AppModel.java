package com.easy.model;

import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.utils.Array;
import com.easy.EasyDebug;
import com.easy.utils.UIBuilder;

public class AppModel {
	
	private Preferences saveData;
	
	private ShopModel shop;
	private OptionsModel options;
	private CacheModel cache;
	
	public AppModel () {
		
		
		//get a save instance for the data - should be empty on first run
		saveData = Gdx.app.getPreferences("easyAppData");
		
		shop = new ShopModel(saveData);
		options = new OptionsModel(saveData);
		cache = new CacheModel(saveData);
	}
					

	
	
	public void saveModelData () {
		shop.saveModelData();
		options.saveModelData();
		saveData.flush();
	}
	
	
	public ShopModel getShopModel () {
		return shop;
	}
	public OptionsModel getOptionsModel () {
		return options;
	}
	public CacheModel getCacheModel () {
		return cache;
	}
	
}
