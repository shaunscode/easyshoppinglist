package com.easy.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class Constants {
	
	public static final float VERSION = 0.1f;
	
	public static final Viewport VIEW_PORT = new StretchViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
	
	public static final String CACHE_SELECTED_MENU = "cacheSelectedMenu";
	public static final String CACHE_DIALOG = "cacheDialog";
	public static final String CACHE_CATALOG_FILTER = "cacheCatalogFilterShowing";
	
	public static final int FONT_SIZE_SM = 16;
	public static final int FONT_SIZE_MD = 22;
	public static final int FONT_SIZE_LG = 28;
	
	public static final float ICON_SIZE_SM = Gdx.graphics.getWidth() / 8.0f;
	public static final float ICON_SIZE_MD = Gdx.graphics.getWidth() / 6.25f;
	public static final float ICON_SIZE_LG = Gdx.graphics.getWidth() / 5.5f;
	
	public static final String SHOP_DATA = "easyPreferenceShopData";
	public static final String CALC_DATA = "easyPreferenceCalcData";
	public static final String OPTIONS_DATA = "easyPreferenceOptionsData";
	public static final String STATE_DATA = "easyPreferenceStateData";
	
	public static final String OPTION_GROUP_CATALOG = "optionGroupSelectedCatalogItems";
	public static final String OPTION_GROUP_LIST = "optionGroupSelectedListItems";
		
	public static final float FADE_DURATION = 0.25f;
	
	public static final Color COLOR_BLACK = Color.BLACK;
	public static final Color COLOR_LIGHT_BLACK = new Color(0.25f, 0.25f ,0.25f, 1.0f);
	public static final Color COLOR_LIGHT_GREY = new Color(0.7f, 0.7f, 0.7f, 1.0f);
	public static final Color COLOR_ORANGE = new Color(1.0f, 0.415f ,0.0f, 1.0f);
	public static final Color COLOR_GREEN = new Color(0.31f, 0.89f ,0.23f, 1.0f);
	public static final Color COLOR_RED = new Color(1, 0.28f, 0.28f, 1);
	public static final Color COLOR_BLUE = new Color(0.2666f, 0.4901f, 0.898f, 1);
	public static final Color COLOR_YELLOW = new Color(1, 0.8235f, 0, 1);
	
	public static final Color COLOR_LABELS = COLOR_LIGHT_BLACK;
	
	//100% / 2560pixels, the height reso of my s7 I test on. scales height depending on h reso of running device. 
	//TODO: properly fix this to use libgdx's Gdx.graphics.getDensity() instead
	public static final float SCALE_HEIGHT = (1.0f / 2560.0f) * Gdx.graphics.getHeight();
	public static final float SCALE_WIDTH = (1.0f / 1440.0f) * Gdx.graphics.getWidth();
	
	public static final int LABEL_PADDING_TOP = (int)(50 * SCALE_HEIGHT);
	public static final int LABEL_PADDING_RIGHT = (int) (90 * SCALE_WIDTH);
	public static final int LABEL_PADDING_BOTTOM = (int)(50 * SCALE_HEIGHT);
	public static final int LABEL_PADDING_LEFT = (int) (90 * SCALE_WIDTH);
	
	public static final float POPUP_SHOW_TIME = 2.0f; //2secs
	
	
	
		
}
