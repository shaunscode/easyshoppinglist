package com.easy;

import com.easy.framework.EasyAction;
import com.easy.framework.EasyView;

public class EasyDebug {
	private static long startTime = 0;
	
	
	
	public static void startTiming () {
		startTime = System.currentTimeMillis();
	}
	
	public static float getTime () {
		return (System.currentTimeMillis() - startTime);
	}
	
	public static void logTime (String message) {
		System.out.println("### Debug.logTime:: " + message + " @ " + getTime() + " msecs since start");
	}

	public static void log (String message) {
		System.out.println("### Debug.log:: " + message);
	}
	
	public static void eventHandled (EasyView view, EasyAction event) {
		System.out.println("## eventHandled @ EasyDebug:  '" + event.getEasyActionType());
	}
}
